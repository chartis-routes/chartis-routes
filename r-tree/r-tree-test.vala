
public class TestRTree : Object {

	public static void test_create_instance () {
		new Spatial.RTree<string> ();
		new Spatial.RTree<int> ();
	}

	public static void test_insert () {
		var rtree = new Spatial.RTree<string> ();
		string[] results;
		//debug ("Inserting point.");
		rtree.insert ({0.0, 0.0, 0.0, 0.0}, "abc");

		//debug ("Searching point.");
		results = (string[]) rtree.search ({-1.0, -1.0, 2.0,  2.0});
		assert (results.length == 1);
		assert (results[0] == "abc");

		rtree.insert ({1.0, 1.0, 1.0, 1.0}, "def");

		results = (string[]) rtree.search ({-1.0, -1.0, 2.0,  2.0});
		assert (results.length == 2);
		assert (results[0] == "abc");
		assert (results[1] == "def");

		results = (string[]) rtree.search ({-1.0, -1.0, 0.0,  0.0});
		assert (results.length == 1);
		assert (results[0] == "abc");
		results = (string[]) rtree.search ({1.0, 1.0, 1.0,  1.0});
		assert (results.length == 1);
		assert (results[0] == "def");

		results = (string[]) rtree.search ({2.0, 2.0, 2.0,  2.0});
		assert (results.length == 0);
	}

	public static void test_split_leaf_node () {
		var rtree = new Spatial.RTree<string> ();
		string[] results;
		for (int i=0; i<10; i++) {
			BoundingBox bbox = {
					Random.double_range (-2.0, 0.0),
					Random.double_range (-2.0, 0.0),
					Random.double_range (0.0, 2.0),
					Random.double_range (0.0, 2.0)
			};
			assert (rtree.insert (bbox, i.to_string ()));
		}
		results = (string[]) rtree.search ({-2.0, -2.0, 2.0,  2.0});
		assert (results.length == 10);
	}

	public static void test_split_bunded_node () {
		var rtree = new Spatial.RTree<string> ();
		string[] results;
		for (int i=0; i<10000; i++) {
			BoundingBox bbox = {
					Random.double_range (-100.0, 0.0),
					Random.double_range (-100.0, 0.0),
					Random.double_range (0.0, 100.0),
					Random.double_range (0.0, 100.0)
			};
			assert (rtree.insert (bbox, bbox.to_string ()));
		}
		results = (string[]) rtree.search ({-100.0, -100.0, 100.0,  100.0});
		print ("Results: %d\n", results.length);
		assert (results.length == 10000);
	}

	public static void test_search () {

	}
}

int main (string[] args) {
	Test.init (ref args);
	Test.add_func ("/RTree/create_instance", TestRTree.test_create_instance);
	Test.add_func ("/RTree/insert", TestRTree.test_insert);
	Test.add_func ("/RTree/test_split_leaf_node", TestRTree.test_split_leaf_node);
	Test.add_func ("/RTree/test_split_bunded_node", TestRTree.test_split_bunded_node);
	Test.add_func ("/RTree/test_search", TestRTree.test_search);
	Test.run ();
	return 0;
}
