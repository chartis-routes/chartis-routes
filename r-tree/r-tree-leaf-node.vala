namespace Spatial {
	public class LeafNode : Node {
		int _size = 0;
		BoundedElement[] elements = new BoundedElement[MAX_ELEMENT_SIZE];

		public int size {
			get { return _size; }
		}

		public void clear () {
			_size = 0;
			elements = new BoundedElement[MAX_ELEMENT_SIZE];
		}

		public override string to_string () {
			return "LeafNode [S:%d]".printf (size);
		}

		public void add (owned BoundedElement bounded_element) {
			if (is_full ()) {
				LeafNode partner_leaf = split_node ((owned) bounded_element);
				adjust_tree_after_split (partner_leaf);
			} else {
				add_element ((owned) bounded_element);
				adjust_tree ();
			}
		}

		void add_element (owned BoundedElement bounded_element) {
			if (_size == 0) {
				bounding_box = bounded_element.bounding_box;
			} else {
				bounding_box.extend (bounded_element.bounding_box);
			}
			elements[_size] = (owned) bounded_element;
			_size++;
		}

		public bool is_full () {
			return _size >= MAX_ELEMENT_SIZE;
		}

		public unowned BoundedElement[] get_elements () {
			return elements;
		}

		public bool is_leaf () {
			return true;
		}

		public void adjust_tree_after_split (LeafNode new_leaf) {
			if (!is_root ()) {
				new_leaf.parent = parent;
				parent.add (new_leaf);
			} else {
				BoundedNode new_root = new BoundedNode ();
				new_leaf.parent = new_root;
				this.parent = new_root;
				new_root.add (this);
				new_root.add (new_leaf);
			}
		}

		void adjust_tree () {
			recalculate_bounds ();
		}

		void recalculate_bounds () {
			if (size > 0) {
				bounding_box = elements[0].bounding_box;
				for (int i=1; i<size; i++) {
					bounding_box.extend (elements[i].bounding_box);
				}
			}
		}

		public LeafNode split_node (owned BoundedElement element) {
			int low_index;
			int high_index;
			pick_index_seeds (out low_index, out high_index);

			LeafNode new_leaf = new LeafNode ();
			LeafNode[] leafs = new LeafNode[] {this, new_leaf};

			BoundedElement[] old_elements = (owned) elements;
			int old_size = _size;
			clear ();

			this.add ((owned) old_elements[low_index]);
			new_leaf.add ((owned) old_elements[high_index]);

			BoundedElement each_element;
			for (int i=0; i<old_size; i++) {
				if (i == low_index || i == high_index) {
					continue;
				}
				each_element = (owned) old_elements[i];
				if (size < MIN_ELEMENT_SIZE) {
					this.add ((owned) each_element);
				} else if (new_leaf.size < MIN_ELEMENT_SIZE) {
					new_leaf.add ((owned) each_element);
				} else {
					LeafNode? best_leaf = get_leaf_with_smallest_area (leafs, each_element.bounding_box);
					if (best_leaf != null) {
						best_leaf.add ((owned) each_element);
					} else {
						error ("Best leaf could not be determined!");
					}
				}
			}
			LeafNode? best_leaf = get_leaf_with_smallest_area (leafs, element.bounding_box);
			if (best_leaf != null) {
				best_leaf.add ((owned) element);
			}
			return new_leaf;
		}

		public static LeafNode? get_leaf_with_smallest_area (LeafNode[] nodes, BoundingBox bbox) {
			double best_area = double.INFINITY;
			LeafNode best_node = null;
			foreach (unowned LeafNode each in nodes) {
				double area = each.bounding_box.area_size_with (bbox);
				if (area < best_area) {
					best_node = each;
					best_area = area;
				}
			}
			return best_node;
		}

		double calculate_separation () {
			BoundingBox bounding_box = elements[0].bounding_box;
			Point lowest = bounding_box.low ();
			Point highest = bounding_box.high ();

			for (int i=1; i<size; i++) {
				bounding_box = elements[i].bounding_box;
				if (bounding_box.low ().compare_to (lowest) < 0) {
					lowest = bounding_box.low ();
				}

				if (bounding_box.high ().compare_to (highest) > 0) {
					highest = bounding_box.high ();
				}
			}
			return lowest.distance (highest);
		}

		void pick_index_seeds (out int low_index, out int high_index) {
			double separation = calculate_separation();

			double normalized_height = separation / bounding_box.height ();
			double normalized_width = separation / bounding_box.width ();

			BoundingBox each_bounding_box = elements[0].bounding_box;
			double each_separation = Math.fabs (each_bounding_box.south - normalized_height) + Math.fabs (each_bounding_box.west - normalized_width);
			low_index = 0;
			high_index = 0;
			double low_separation = each_separation;
			double high_separation = each_separation;

			for (int i=1; i<size; i++) {
				each_bounding_box = elements[i].bounding_box;
				each_separation = Math.fabs (each_bounding_box.south - normalized_height) + Math.fabs (each_bounding_box.west - normalized_width);
				if (each_separation < low_separation) {
					low_separation = each_separation;
					low_index = i;
				} else if (each_separation > high_separation) {
					high_separation = each_separation;
					high_index = i;
				}
			}
		}

		public ElementsIterator elements_iterator () {
			return new ElementsIterator (this);
		}

		public class ElementsIterator {
			int index = 0;
			LeafNode leaf;

			public ElementsIterator (LeafNode leaf) {
				this.leaf = leaf;
			}

			public unowned BoundedElement? next_value () {
				if (index >= leaf.size) {
					return null;
				}
				index++;
				return leaf.elements[index-1];
			}

			public ElementsIterator iterator () {
				return this;
			}
		}
	}
}
