
namespace Spatial {

	[Compact]
	public class BoundedElement<T> {
		public BoundingBox bounding_box;
		public T element;

		public BoundedElement (BoundingBox bounding_box, owned T element) {
			this.bounding_box = bounding_box;
			this.element = element;
		}
	}

	public abstract class Node {
		public BoundingBox bounding_box;

		public const int MAX_SUB_NODE_SIZE = 52;
		public const int MIN_SUB_NODE_SIZE = 2;
		public const int MAX_ELEMENT_SIZE = 52;
		public const int MIN_ELEMENT_SIZE = 2;

		protected BoundedNode parent = null;

		public abstract string to_string ();

		public unowned Node get_parent () {
			return parent;
		}

		public bool is_root () {
			return parent == null;
		}

		public virtual Node? get_sub_node_with_smallest_area (BoundingBox bbox) {
			return null;
		}
	}

	public class RTree<T> : Object {
		internal Node root = new LeafNode ();

		LeafNode? choose_leaf (BoundingBox bbox) {
			if (root is LeafNode) {
				return root as LeafNode;
			} else {
				Node current_node = root;
				while (current_node is BoundedNode) {
					current_node = current_node.get_sub_node_with_smallest_area (bbox);
				}
				return current_node as LeafNode;
			}
		}

		public void check_root () {
			while (!root.is_root ()) {
				root = root.get_parent ();
			}
		}

		public bool insert (BoundingBox bbox, T element) {
			var bounded_element = new BoundedElement<T> (bbox, element);
			LeafNode leaf = choose_leaf (bbox);
			leaf.add ((owned) bounded_element);
			check_root ();
			return true;
		}

		public T[] search (BoundingBox bbox) {
			T[] elements = new T[] {};

			Gee.List<LeafNode> leaf_list = get_leafs_for (bbox);

			foreach (LeafNode each_leaf in leaf_list) {
				foreach (unowned BoundedElement bounded_element in each_leaf.elements_iterator ()) {
					if (bbox.overlaps (bounded_element.bounding_box)) {
						unowned BoundedElement<T> full_bounded_element = (BoundedElement<T>) bounded_element;
						elements += full_bounded_element.element;
					}
				}
			}
			return elements;
		}

		Gee.List<unowned LeafNode> get_leafs_for (BoundingBox bbox) {
			var result_list = new Gee.ArrayList<LeafNode> ();
			recurse_to_get_leafs (bbox, root, result_list);
			return result_list;
		}

		void recurse_to_get_leafs (BoundingBox bbox, Node node, Gee.List<unowned LeafNode> list) {
			if (node is LeafNode) {
				list.add (node as LeafNode);
			} else {
				BoundedNode current = node as BoundedNode;
				foreach (var sub_node in current.sub_nodes_iterator ()) {
					if (bbox.overlaps (sub_node.bounding_box) || sub_node.bounding_box.overlaps (bbox)) {
						recurse_to_get_leafs (bbox, sub_node, list);
					}
				}
			}
		}
	}
}
