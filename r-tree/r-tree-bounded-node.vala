namespace Spatial {
	public class BoundedNode : Node {
		int _size = 0;
		Node[] sub_nodes = new Node[MAX_SUB_NODE_SIZE];

		public int size {
			get { return _size; }
		}

		public override string to_string () {
			return "BoundedNode [S:%d]".printf (size);
		}

		public void add (Node node) {
			if (is_full ()) {
				BoundedNode partner_node = split_node (node);
				adjust_tree_after_split (partner_node);
			} else {
				add_node (node);
				adjust_tree ();
			}
		}

		void adjust_tree_after_split (BoundedNode new_node) {
			if (!is_root ()) {
				new_node.parent = parent;
				parent.add (new_node);
			} else {
				BoundedNode new_root = new BoundedNode ();
				new_node.parent = new_root;
				this.parent = new_root;
				new_root.add (this);
				new_root.add (new_node);
			}
		}

		void add_to_best_node (BoundedNode node1, BoundedNode node2, Node node_to_add) {
			double area1 = node1.bounding_box.area_size_with (node_to_add.bounding_box);
			double area2 = node2.bounding_box.area_size_with (node_to_add.bounding_box);
			if (area1 < area2) {
				node1.add (node_to_add);
			} else if (area2 < area1){
				node2.add (node_to_add);
			} else {
				// TODO - decide on node size istead
				node2.add (node_to_add);
			}
		}

		public void clear () {
			sub_nodes = new Node[MAX_SUB_NODE_SIZE];
			_size = 0;
		}

		BoundedNode split_node (Node node_to_add) {
			int low_index;
			int high_index;
			pick_index_seeds (out low_index, out high_index);

			BoundedNode new_node = new BoundedNode ();

			Node[] old_sub_nodes = sub_nodes;
			int old_size = _size;
			clear ();

			this.add (old_sub_nodes[low_index]);
			new_node.add (old_sub_nodes[high_index]);

			Node each_node;
			for (int i=0; i<old_size; i++) {
				if (i == low_index || i == high_index) {
					continue;
				}
				each_node = old_sub_nodes[i];
				if (size < MIN_SUB_NODE_SIZE) {
					this.add (each_node);
				} else if (new_node.size < MIN_SUB_NODE_SIZE) {
					new_node.add (each_node);
				} else {
					add_to_best_node (this, new_node, each_node);
				}
			}
			add_to_best_node (this, new_node, node_to_add);
			return new_node;
		}

		void pick_index_seeds (out int low_index, out int high_index) {
			double separation = calculate_separation();

			double normalized_height = separation / bounding_box.height ();
			double normalized_width = separation / bounding_box.width ();

			BoundingBox each_bounding_box = sub_nodes[0].bounding_box;
			double each_separation = Math.fabs (each_bounding_box.south - normalized_height) + Math.fabs (each_bounding_box.west - normalized_width);
			low_index = 0;
			high_index = 0;
			double low_separation = each_separation;
			double high_separation = each_separation;

			for (int i=1; i<size; i++) {
				each_bounding_box = sub_nodes[i].bounding_box;
				each_separation = Math.fabs (each_bounding_box.south - normalized_height) + Math.fabs (each_bounding_box.west - normalized_width);
				if (each_separation < low_separation) {
					low_separation = each_separation;
					low_index = i;
				} else if (each_separation > high_separation) {
					high_separation = each_separation;
					high_index = i;
				}
			}
		}

		double calculate_separation () {
			BoundingBox bounding_box = sub_nodes[0].bounding_box;
			Point lowest = bounding_box.low ();
			Point highest = bounding_box.high ();

			for (int i=1; i<size; i++) {
				bounding_box = sub_nodes[i].bounding_box;
				if (bounding_box.low ().compare_to (lowest) < 0) {
					lowest = bounding_box.low ();
				}

				if (bounding_box.high ().compare_to (highest) > 0) {
					highest = bounding_box.high ();
				}
			}
			return lowest.distance (highest);
		}

		void add_node (Node node) {
			if (_size == 0) {
				bounding_box = node.bounding_box;
			} else {
				bounding_box.extend (node.bounding_box);
			}
			sub_nodes[_size] = node;
			_size++;
		}

		public bool is_full () {
			return _size >= MAX_SUB_NODE_SIZE;
		}

		public unowned Node[] get_sub_nodes () {
			return sub_nodes;
		}

		public bool is_leaf () {
			return false;
		}

		public override Node? get_sub_node_with_smallest_area (BoundingBox bbox) {
			if (size <= 0) {
				return null;
			}
			double best_area = sub_nodes[0].bounding_box.area_size_with (bbox);
			Node best_node = sub_nodes[0];
			for (int i=1; i<size; i++) {
				double area = sub_nodes[i].bounding_box.area_size_with (bbox);
				if (area < best_area) {
					best_node = sub_nodes[i];
					best_area = area;
				}
			}
			return best_node;
		}

		void adjust_tree () {
			if (!is_root ()) {
				recalculate_bounds ();
			}
		}

		void recalculate_bounds () {
			if (size > 0) {
				bounding_box = sub_nodes[0].bounding_box;
				for (int i=1; i<size; i++) {
					bounding_box.extend (sub_nodes[i].bounding_box);
				}
			}
		}

		public SubNodesIterator sub_nodes_iterator () {
			return new SubNodesIterator (this);
		}

		public class SubNodesIterator {
			int index = 0;

			BoundedNode node;

			public SubNodesIterator (BoundedNode node) {
				this.node = node;
			}

			public Node? next_value () {
				if (index >= node.size) {
					return null;
				}
				Node sub_node = node.sub_nodes[index];
				index++;
				return sub_node;
			}

			public SubNodesIterator iterator () {
				return this;
			}
		}
	}
}
