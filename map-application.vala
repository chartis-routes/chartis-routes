using Gtk;

public class FindRouteDialog : Dialog {
	Entry latitude1_entry;
	Entry longitude1_entry;

	Entry latitude2_entry;
	Entry longitude2_entry;



	construct {
		set_modal (true);
		add_buttons (Stock.OK, ResponseType.ACCEPT, Stock.CANCEL, ResponseType.CANCEL, null);
		realize.connect (on_realize);
	}

	void on_realize () {
		Box main_box = new VBox (false, 0);
		Label label;

		Box box = new HBox (false, 0);
		latitude1_entry = new Entry ();
		longitude1_entry = new Entry();
		label = new Label ("Point 1");
		label.set_width_chars (15);
		label.set_alignment (0.0f, 0.0f);
		box.pack_start (label, false, false, 0);
		box.pack_start (latitude1_entry, true, true, 0);
		box.pack_start (longitude1_entry, true, true, 0);
		main_box.pack_start (box, true, true, 0);

		box = new HBox (false, 0);
		latitude2_entry = new Entry ();
		longitude2_entry = new Entry();
		label = new Label ("Point 2");
		label.set_width_chars (15);
		label.set_alignment (0.0f, 0.0f);
		box.pack_start (label, false, false, 0);
		box.pack_start (latitude2_entry, true, true, 0);
		box.pack_start (longitude2_entry, true, true, 0);
		main_box.pack_start (box, true, true, 0);

		var content_area = get_content_area () as Box;
		content_area.pack_start (main_box, true, true, 0);

		show_all ();
	}

	public Point get_point_1 () {
		return {
			double.parse (latitude1_entry.get_text ()),
			double.parse (longitude1_entry.get_text ())
		};
	}

	public Point get_point_2 () {
		return {
			double.parse (latitude2_entry.get_text ()),
			double.parse (longitude2_entry.get_text ())
		};
	}
}

public class RouteMarker : Champlain.CustomMarker {
	int marker_size;
	double red;
	double green;
	double blue;

	public RouteMarker (int marker_size = 8, double red = 0.1, double green = 0.1, double blue = 0.9) {
		GLib.Object();
		realize.connect (on_realize);
		this.marker_size = marker_size;
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	void on_realize () {
		Clutter.CairoTexture background = new Clutter.CairoTexture (marker_size, marker_size);
		Cairo.Context context = background.create ();
		context.set_source_rgb (0, 0, 0);
		context.arc (marker_size / 2.0, marker_size / 2.0, marker_size / 2.0, 0, 2 * Math.PI);
		context.close_path ();

		context.set_source_rgba (red, green, blue, 1.0);
		context.fill ();

		this.add (background);
		background.set_anchor_point_from_gravity (Clutter.Gravity.CENTER);
		background.set_position (0, 0);
	}
}

public class ChartisApplication : Window {
	Champlain.View view;

	Champlain.PathLayer route_polygon = new Champlain.PathLayer ();
	Champlain.PathLayer route_polygon_smart_ignorant_finder = new Champlain.PathLayer ();
	Champlain.PathLayer route_polygon_ignorant_finder = new Champlain.PathLayer ();
	Champlain.PathLayer route_polygon_smart_finder = new Champlain.PathLayer ();

	Gee.List<Champlain.PathLayer> route_polygons = new Gee.ArrayList<Champlain.PathLayer> ();

	bool route_start = true;

	Champlain.MarkerLayer visited_layer = new Champlain.MarkerLayer ();
	bool visited_points_visible = true;

	Point start_location;
	Point click_location;

	Champlain.Marker start_marker;
	Champlain.Marker end_marker;

	DiskNetwork network;

	ComboBox combo = new ComboBox ();
	Champlain.MapSource source;

	int color_index;

	construct {
		title = "Chartis";
		set_default_size (1024, 768);
		destroy.connect (Gtk.main_quit);

		var view_box = new Gtk.VBox (false, 0);

		var champlain_embed = new GtkChamplain.Embed ();

		view_box.pack_start (champlain_embed, true, true, 0);
		view = champlain_embed.get_view ();
		view.add_layer (route_polygon);
		route_polygon.set_stroke_color (Clutter.Color() {red = 0x00, green = 0xff, blue = 0x00, alpha = 0xff});

		view.add_layer (route_polygon_smart_ignorant_finder);
		route_polygon_smart_ignorant_finder.set_stroke_color (Clutter.Color() {red = 0x00, green = 0xff, blue = 0x00, alpha = 0xff});

		view.add_layer (route_polygon_ignorant_finder);
		route_polygon_ignorant_finder.set_stroke_color (Clutter.Color() {red = 0xff, green = 0x00, blue = 0xff, alpha = 0xff});

		view.add_layer (route_polygon_smart_finder);
		route_polygon_smart_finder.set_stroke_color (Clutter.Color() {red = 0x33, green = 0xff, blue = 0xff, alpha = 0xff});

		view.zoom_on_double_click = false;
		view.reactive = true;

		view.set_zoom_level (13);
		view.center_on (46.556, 15.646);
		//view.center_on (26.194877, 127.720699);

		view.button_release_event.connect (on_button_press);
		view.add_layer (visited_layer);

		var route_marker_layer = new Champlain.MarkerLayer ();
		start_marker = new RouteMarker ();
		route_marker_layer.add_marker (start_marker);
		end_marker = new RouteMarker ();
		route_marker_layer.add_marker (end_marker);

		view.add_layer (route_marker_layer);

		var tool_box = new HBox (false, 0);
		add_toolbox_content (tool_box);
		view_box.pack_start (tool_box, false, false, 0);

		add (view_box);

		open ();
	}

	void add_toolbox_content (Box toolbox) {
		var open_gpx_button = new Button.with_label ("Open GPX");
		open_gpx_button.clicked.connect (on_open_gpx_clicked);
		toolbox.pack_start (open_gpx_button, false, false, 0);

		var open_csv_button = new Button.with_label ("Open CSV");
		open_csv_button.clicked.connect (on_open_csv_clicked);
		toolbox.pack_start (open_csv_button, false, false, 0);

		var calculate_route_button = new Button.with_label ("Find Route");
		calculate_route_button.clicked.connect (on_calculate_route_clicked);
		toolbox.pack_start (calculate_route_button, false, false, 0);

		combo = new Gtk.ComboBox ();
		build_combo_box ();
		combo.set_active (0);
		combo.changed.connect (on_combo_changed);
		toolbox.pack_start (combo, false, false, 0);

		var points_button = new Button.with_label ("Show/Hide Points");
		points_button.clicked.connect (on_points_clicked);
		toolbox.pack_start (points_button, false, false, 0);
	}

	void on_open_gpx_clicked () {
		var dialog = new FileChooserDialog ("Choose GPX", this, FileChooserAction.OPEN, Stock.OPEN, ResponseType.ACCEPT, Stock.CANCEL, ResponseType.CANCEL, null);
		dialog.set_current_folder (Environment.get_current_dir ());
		if (dialog.run () == ResponseType.ACCEPT) {
			try {
				draw_gpx (dialog.get_filename ());
			} catch (Error er) {
				error (er.message);
			}
		}
		dialog.destroy ();
	}

	void on_open_csv_clicked () {
		var dialog = new FileChooserDialog ("Choose CSV", this, FileChooserAction.OPEN, Stock.OPEN, ResponseType.ACCEPT, Stock.CANCEL, ResponseType.CANCEL, null);
		dialog.set_current_folder (Environment.get_current_dir ());
		if (dialog.run () == ResponseType.ACCEPT) {
			try {
				draw_csv (dialog.get_filename ());
			} catch (Error er) {
				error (er.message);
			}
		}
		dialog.destroy ();
	}

	Champlain.PathLayer create_new_path_layer (Clutter.Color color) {
		Champlain.PathLayer route_polygon = new Champlain.PathLayer ();
		view.add_layer (route_polygon);
		route_polygon.set_stroke_color (color);
		route_polygon.set_stroke_width (5.0);
		route_polygons.add (route_polygon);
		return route_polygon;
	}

	Clutter.Color get_color () {
		color_index++;
		switch (color_index % 5) {
			case 0:
				return Clutter.Color() {red = 0x33, green = 0xff, blue = 0xff, alpha = 0xb0};
			case 1:
				return Clutter.Color() {red = 0xff, green = 0x00, blue = 0x00, alpha = 0xb0};
			case 2:
				return Clutter.Color() {red = 0x00, green = 0xff, blue = 0x00, alpha = 0xb0};
			case 3:
				return Clutter.Color() {red = 0x00, green = 0x00, blue = 0xff, alpha = 0xb0};
			case 4:
				return Clutter.Color() {red = 0xff, green = 0xff, blue = 0x00, alpha = 0xb0};
		}
		return Clutter.Color() {red = 0x33, green = 0xff, blue = 0xff, alpha = 0xb0};
	}

	public void draw_gpx (string file_path) throws Error {
		Champlain.PathLayer route_polygon = create_new_path_layer (get_color ());

		var parser = new Gpx.Parser ();
		var root = parser.parse_file (file_path);

		double distance = 0.0;
		bool previous = false;
		double previous_lat = double.NAN;
		double previous_lon = double.NAN;

		foreach (Gpx.Track track in root.tracks) {
			foreach (Gpx.TrackSegment segment in track.segments) {
				foreach (Gpx.Waypoint point in segment.points) {
					route_polygon.add_node (new Champlain.Coordinate.full (point.latitude,point.longitude));
					if (previous) {
						distance += GeoTools.distance_exact_lat_lon (previous_lat, previous_lon, point.latitude, point.longitude);
					}
					previous_lat = point.latitude;
					previous_lon = point.longitude;
					previous = true;
				}
			}
		}
		print ("Distance: %0.4f\n", distance);
	}

	public void draw_csv (string file_path) throws Error {
		var file_stream = FileStream.open (file_path, "r");
		double distance = 0.0;
		bool previous = false;
		double previous_lat = double.NAN;
		double previous_lon = double.NAN;

		Champlain.PathLayer route_polygon = create_new_path_layer (get_color ());

		for (string line = file_stream.read_line () ; line != null; line = file_stream.read_line ()) {
			var array_of_string = line.replace ("\"", "").replace(",", ";").replace(" ", ";").split (";");

			if (array_of_string.length < 2) {
				continue;
			}

			double latitude;
			if (!double.try_parse (array_of_string[0], out latitude)) {
				continue;
			}

			double longitude;
			if (!double.try_parse (array_of_string[1], out longitude)) {
				continue;
			}

			route_polygon.add_node (new Champlain.Coordinate.full (latitude, longitude));

			if (previous) {
				distance += GeoTools.distance_exact_lat_lon (previous_lat, previous_lon, latitude, longitude);
			}
			previous_lat = latitude;
			previous_lon = longitude;
			previous = true;
		}

		print ("Distance: %0.4f\n", distance);
	}

	void on_calculate_route_clicked() {
		var dialog = new FindRouteDialog ();
		if (dialog.run () == ResponseType.ACCEPT) {
			Point point1 = dialog.get_point_1 ();
			Point point2 = dialog.get_point_2 ();
			calculate_route (point1, point2);
		}
		dialog.destroy ();
	}

	void build_combo_box () {
		Champlain.MapSourceFactory factory = Champlain.MapSourceFactory.dup_default ();
		var store = new Gtk.TreeStore (2, typeof (string), typeof (string));
		Gtk.TreeIter parent;

		foreach (var description in factory.get_registered ()) {
			store.append (out parent, null);
			store.set (parent, 0, description.id, 1, description.name, -1);
		}
		combo.set_model (store);
		var cell = new Gtk.CellRendererText ();
		combo.pack_start (cell, false);
		combo.set_attributes (cell, "text", 1, null);
	}

	void on_combo_changed () {
		Gtk.TreeIter iter;
		if (!combo.get_active_iter (out iter)) {
			return;
		}

		var model = combo.get_model ();
		Value id_value;
		model.get_value (iter, 0, out id_value);
		string id = id_value.get_string ();
		var factory = Champlain.MapSourceFactory.dup_default ();
		source = factory.create_cached_source (id);
		if (source != null) {
			view.set_map_source (source);
		}
	}

	void on_points_clicked () {
		if (visited_points_visible) {
			visited_layer.hide_all_markers ();
		} else {
			visited_layer.show_all_markers ();
		}
		visited_points_visible = !visited_points_visible;
	}

	public void open () {
		try {
			var routing_directory = File.new_for_path("routing-data");
			if (!routing_directory.query_exists ()) {
				print ("Routing data directory does not exists\n");
			}

			network = new DiskNetwork (routing_directory);
		} catch (Error e) {
			error (e.message);
		}
	}

	public bool on_button_press (Clutter.ButtonEvent event) {
		if (event.button == 2) {
			var latitude  = view.y_to_latitude ((uint)event.y);
			var longitude = view.x_to_longitude ((uint)event.x);

			var display = this.get_display ();
			var clipboard = Clipboard.get_for_display (display, Gdk.SELECTION_CLIPBOARD);
			clipboard.set_text ("%f %f".printf (latitude, longitude), -1);

 		} else if (event.button == 3) {
			//Gtk.MenuItem menu_item = null;

			click_location.latitude = view.y_to_latitude ((uint)event.y);
			click_location.longitude = view.x_to_longitude ((uint)event.x);

			if (route_start) {
				print ("Route start!\n");
				on_route_start_activated ();
			} else {
				print ("Route end!\n");
				on_route_end_activated ();
			}

			/*var menu = new Gtk.Menu ();

			if (route_start) {
				menu_item = new Gtk.MenuItem.with_label ("Route start");
				menu_item.activate.connect (on_route_start_activated);

			} else {
				menu_item = new Gtk.MenuItem.with_label ("Route end");
				menu_item.activate.connect (on_route_end_activated);
			}

			menu.add (menu_item);
			menu.show_all ();
			menu.popup (null, null, null, event.button, event.time);*/
		}
		return true;
	}

	public void on_route_start_activated () {
		start_location = click_location;
		route_polygon.remove_all ();
		start_marker.set_location (start_location.latitude, start_location.longitude);

		route_start = false;
	}

	public void on_route_end_activated () {
		calculate_route (start_location, click_location);
		route_start = true;
	}

	public void calculate_route (Point start, Point end) {
		try {
			visited_layer.remove_all ();
			Timer timer = new Timer ();

			Node? start_node = network.find_nearest_node (start);
			print ("Find nearest in start node: %.3f %s\n", timer.elapsed (), start_node.get_location ().to_string ());
			timer.reset ();

			Node? end_node = network.find_nearest_node  (end);
			print ("Find nearest in end node: %.3f %s\n", timer.elapsed (), end_node.get_location ().to_string ());
			timer.reset ();

			if (start_node == null || end_node == null) {
				print ("Nearest node could not be found.\n");
				return;
			}

			start_marker.set_location (start_node.get_location ().latitude, start_node.get_location ().longitude);
			end_marker.set_location (end_node.get_location ().latitude, end_node.get_location ().longitude);

			RouteFinder finder;

			timer.reset ();
			finder = new TwoPointRouteFinderFast ();
			finder.find (start_node, end_node);
			print ("'Two Point' Route Finder found solution in %f. Distance %.3f\n", timer.elapsed (), finder.get_solution_distance ());
			draw_solution (finder, route_polygon_ignorant_finder);
			//draw_nodes (finder, 0.2, 0.9, 0.5);
			write_solution_to_file (finder);

			/*timer.reset ();
			finder = new PathSkippingRouteFinder ();
			finder.find (start_node, end_node);
			print ("'Path skipping' Route Finder found solution in %f. Distance %.3f\n", timer.elapsed (), finder.get_solution_distance ());
			draw_solution (finder, route_polygon_smart_ignorant_finder);
			draw_nodes (finder, 0.9, 0.1, 0.1);*/

			/*timer.reset ();
			finder = new DistanceBasedRouteFinder ();
			finder.find (start_node, end_node);
			print ("Distance based route finder found solution in %f. Distance %.3f\n", timer.elapsed (), finder.get_solution_distance ());
			draw_solution (finder, route_polygon_smart_finder);*/
		} catch (Error e) {
			error (e.message);
		}
	}

	public void draw_solution (RouteFinder finder, Champlain.PathLayer route_polygon) throws Error {
		route_polygon.remove_all ();

		bool previous = false;
		double previous_lat = 0.0;
		double previous_lon = 0.0;
		double distance = 0.0;

		foreach (Node node in finder.get_solution ()) {
			double latitude = node.get_location ().latitude;
			double longitude = node.get_location ().longitude;

			route_polygon.add_node (new Champlain.Coordinate.full (latitude, longitude));

			if (previous) {
				distance += GeoTools.distance_exact_lat_lon (previous_lat, previous_lon, latitude, longitude);
			}

			previous_lat = latitude;
			previous_lon = longitude;
			previous = true;
		}
		print ("Distance: %.4f\n", distance);
	}

	public void write_solution_to_file (RouteFinder finder) throws Error {
		var file = File.new_for_path("result.csv");
		if (file.query_exists ()) {
			file.delete ();
		}
		var io_stream = file.create_readwrite (FileCreateFlags.REPLACE_DESTINATION);
		var output_stream = io_stream.get_output_stream ();
		var data_output_stream = new DataOutputStream (output_stream);

		foreach (Node node in finder.get_solution ()) {
			route_polygon.add_node (new Champlain.Coordinate.full (node.get_location ().latitude, node.get_location ().longitude));
			data_output_stream.put_string ("%.6f;%.6f\n".printf (node.get_location ().latitude, node.get_location ().longitude));
		}
	}

	public void draw_nodes (RouteFinder finder, double red, double green, double blue) throws Error {
		foreach (Node node in finder.get_visited_nodes ()) {
			var marker = new RouteMarker (4, red, green, blue);
			marker.set_location (node.get_location ().latitude, node.get_location ().longitude);
			visited_layer.add_marker (marker);
		}
	}
}

public int main (string[] args) {
	GtkClutter.init (ref args);
	Gtk.init  (ref args);

	if (!Thread.supported()) {
		print ("Cannot run without threads.\n");
		return 1;
	}

	var application = new ChartisApplication ();
	application.show_all ();

	Gtk.main ();

	return 0;
}
