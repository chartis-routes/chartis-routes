public class TestRTreeDisk : Object {

	public static void assert_equals_double (double expected, double result) {
		if (expected != result) {
			error ("Assert failed - expected: %f but is: %f", expected, result);
		}
	}

	public static void assert_equals_int (int expected, int result) {
		if (expected != result) {
			error ("Assert failed - expected: %d but is: %d", expected, result);
		}
	}

	public static void test_create_instance () {
		try {
			var file = File.new_for_path("test.data");
			if (file.query_exists ()) {
				file.delete ();
			}
			var rtree = new Spatial.RTreeDisk (file);
			rtree.close ();
		} catch (Error e) {
			error (e.message);
		}
	}

	public static void test_insert_zero_coordinates () {
		try {
			var file = File.new_for_path("test.data");
			if (file.query_exists ()) {
				file.delete ();
			}
			var rtree = new Spatial.RTreeDisk (file);

			for (int i=0; i<210; i++) {
				BoundingBox bbox = { 0.0,0.0, 0.0, 0.0 };
				rtree.insert (bbox, -i);
			}

			Spatial.BoundedPointer[] results;
			results = rtree.search ({0.0, 0.0, 0.0,  0.0});
			assert (results.length == 210);

			rtree.close ();
		} catch (Error e) {
			error (e.message);
		}
	}

	public static void test_brust_insert () {
		try {
			var file = File.new_for_path("test.data");
			if (file.query_exists ()) {
				file.delete ();
			}
			var rtree = new Spatial.RTreeDisk (file);

			for (int i=0; i<100000; i++) {
				BoundingBox bbox = {
						Random.double_range (-100.0, 0.0),
						Random.double_range (-100.0, 0.0),
						Random.double_range (0.0, 100.0),
						Random.double_range (0.0, 100.0)
				};
				rtree.insert (bbox, -i);
			}
			rtree.close ();
		} catch (Error e) {
			error (e.message);
		}
	}

	public static void test_split_root_node () {
		Spatial.RTreeDisk rtree;
		try {
			var file = File.new_for_path("test.data");
			if (file.query_exists ()) {
				file.delete ();
			}
			rtree = new Spatial.RTreeDisk (file);

			Spatial.BoundedPointer[] results;

			assert (rtree.insert ({-11.0, -11.0, -11.0, -11.0}, 1000));
			assert (rtree.insert ({-11.0, -11.0, -11.0, -11.0}, 2000));
			assert (rtree.insert ({-11.0, -11.0, -11.0, -11.0}, 3000));
			assert (rtree.insert ({-11.0, -11.0, -11.0, -11.0}, 4000));

			int numer_of_points = 2000;
			for (int i=0; i<numer_of_points; i++) {
				BoundingBox bbox = {
						Random.double_range (-10.0, 0.0),
						Random.double_range (-10.0, 0.0),
						Random.double_range (0.0, 10.0),
						Random.double_range (0.0, 10.0)
				};
				assert (rtree.insert (bbox, i));
			}

			assert (rtree.insert ({11.0, 11.0, 11.0, 11.0}, 100));
			assert (rtree.insert ({11.0, 11.0, 11.0, 11.0}, 200));
			assert (rtree.insert ({11.0, 11.0, 11.0, 11.0}, 300));

			results = rtree.search ({-10.0, -10.0, 10.0,  10.0});
			assert (results.length == numer_of_points);

			results = rtree.search ({-100.0, -100.0, 100.0, 100.0});
			assert (results.length == numer_of_points + 7);

			results = rtree.search ({-11.0, -11.0, -11.0, -11.0});
			assert_equals_int (4, results.length);

			results = rtree.search ({10.5, 10.5, 11.5, 11.5});
			assert_equals_int (3, results.length);

			rtree.close ();

			rtree = new Spatial.RTreeDisk (file);

			//results = rtree.search ({-10.0, -10.0, 10.0,  10.0});
			//assert (results.length == numer_of_points);

			//results = rtree.search ({-100.0, -100.0, 100.0, 100.0});
			//assert (results.length == numer_of_points + 3);

			//results = rtree.search ({11.0, 11.0, 11.0, 11.0});
			//assert (results.length == 3);

			var timer = new Timer ();
			results = rtree.search ({-5.0, -5.0, 5.0, 5.0});
			print ("Results: %d in: %f\n", results.length, timer.elapsed ());

			rtree.close ();

		} catch (Error e) {
			error (e.message);
		}
	}

	public static void test_insert () {
		try {
			var file = File.new_for_path("test.data");
			if (file.query_exists ()) {
				file.delete ();
			}
			Spatial.RTreeDisk rtree;

			rtree = new Spatial.RTreeDisk (file);
			Spatial.BoundedPointer[] results;

			rtree.insert ({0.0, 0.0, 0.0, 0.0}, 16);

			results = rtree.search ({-2.0, -2.0, 2.0,  2.0});
			assert (results.length == 1);
			assert (results[0].position == 16);

			rtree.insert ({1.0, 1.0, 1.0, 1.0}, 32);

			results =  rtree.search ({-1.0, -1.0, 2.0,  2.0});
			assert (results.length == 2);

			results = rtree.search ({-1.0, -1.0, 0.0,  0.0});
			assert (results.length == 1);
			assert (results[0].position == 16);

			results = rtree.search ({0.0, 0.0, 0.0,  0.0});
			assert (results.length == 1);
			assert (results[0].position == 16);

			results = rtree.search ({1.0, 1.0, 1.0, 1.0});
			assert (results.length == 1);
			assert (results[0].position == 32);

			results = rtree.search ({2.0, 2.0, 2.0,  2.0});
			assert (results.length == 0);

			rtree.close ();

			rtree = new Spatial.RTreeDisk (file);

			results =  rtree.search ({-1.0, -1.0, 2.0,  2.0});
			assert (results.length == 2);

			results = rtree.search ({-1.0, -1.0, 0.0,  0.0});
			assert (results.length == 1);
			assert (results[0].position == 16);

			results = rtree.search ({0.0, 0.0, 0.0,  0.0});
			assert (results.length == 1);
			assert (results[0].position == 16);

			results = rtree.search ({1.0, 1.0, 1.0, 1.0});
			assert (results.length == 1);
			assert (results[0].position == 32);

			results = rtree.search ({2.0, 2.0, 2.0,  2.0});
			assert (results.length == 0);

		} catch (Error e) {
			error (e.message);
		}
	}

	public static void add_tests () {
		Test.add_func ("/RTree Disk/create_instance", TestRTreeDisk.test_create_instance);
		Test.add_func ("/RTree Disk/insert", TestRTreeDisk.test_insert);
		Test.add_func ("/RTree Disk/split_root_node", TestRTreeDisk.test_split_root_node);
		Test.add_func ("/RTree Disk/insert_zero_coordinates", TestRTreeDisk.test_insert_zero_coordinates);
		Test.add_func ("/RTree Disk/brust_insert", TestRTreeDisk.test_brust_insert);
	}
}
