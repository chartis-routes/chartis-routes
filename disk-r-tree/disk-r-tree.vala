
namespace Spatial {

	public class RTreeDisk : Object {
		DiskNodeHandler disk_node_handler;
		Node root;
		bool closed = false;

		public RTreeDisk (File file) throws Error {
				disk_node_handler = new DiskNodeHandler.from_file (file);
				root = disk_node_handler.get_node_at_position (0);
		}

		public bool insert (BoundingBox bbox, int64 element) throws Error {
			return insert_pointer (BoundedPointer (bbox, (int32) element));
		}

		bool insert_pointer (BoundedPointer pointer) throws Error {
			//print (@"Inserting $(pointer)\n");
			root.insert_into_leaf (pointer);
			return true;
		}

		public BoundedPointer[] search (BoundingBox bbox) throws Error {
			BoundedPointer[] pointers = new BoundedPointer[] {};

			Gee.List<Node> leaf_list = get_leafs_for (bbox);

			foreach (Node each_leaf in leaf_list) {
				foreach (BoundedPointer each in each_leaf.pointer_iterator ()) {
					//print (@"Node $(each.get_bounding_box ()) - $(each.position)\n");
					if (bbox.overlaps (each.get_bounding_box ())) {
						pointers += each;
					}
				}
			}
			return pointers;
		}

		Gee.List<Node> get_leafs_for (BoundingBox bbox) throws Error {
			var result_list = new Gee.ArrayList<Node> ();
			recurse_to_get_leafs (bbox, root, result_list);
			return result_list;
		}

		void recurse_to_get_leafs (BoundingBox bbox, Node node, Gee.List<Node> list) throws Error {
			if (node.is_leaf ()) {
				list.add (node);
			} else {
				foreach (var each in node.pointer_iterator ()) {
					//print (@"Node $(each.get_bounding_box ()) - $(each.position)\n");
					if (bbox.intersect (each.get_bounding_box ()) != BoundingBox.invalid ()) {
						Node sub_node = disk_node_handler.get_node_at_position (each.position);
						recurse_to_get_leafs (bbox, sub_node, list);
					}
				}
			}
		}

		public void close () throws Error {
			if (!closed) {
				disk_node_handler.close ();
			}
			closed = true;
		}

		public void info () {
			print (@"Size of r-tree node: $(sizeof(NodeContent))\n");
		}
	}
}
