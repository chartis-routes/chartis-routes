namespace Spatial {

	public interface Serializable {
		public abstract uint8[] get_bytes ();
	}

	public enum NodeType {
		LEAF,TREE;
	}

	public struct BoundedPointer {
		public float south;
		public float west;
		public float north;
		public float east;
		public int32 position;


		public BoundedPointer (BoundingBox bounding_box, int32 position) {
			this.set_bounding_box (bounding_box);
			this.position = position;
		}

		public BoundingBox get_bounding_box () {
			return BoundingBox() {
				south = (double) south,
				west = (double) west,
				north = (double) north,
				east = (double) east
			};
		}

		public void set_bounding_box (BoundingBox bounding_box) {
			this.south = (float) bounding_box.south;
			this.west = (float) bounding_box.west;
			this.north = (float) bounding_box.north;
			this.east = (float) bounding_box.east;
		}

		public void set_position (int64 position) {
			this.position = (int32) position;
		}

		public string to_string () {
			return @"Bound: $(get_bounding_box ()) - $(position)";
		}
	}

	public struct NodeContent {
		public uint16 type; // 2B
		public uint16 size; // 2B
		public int32 parent; // 4B
		public BoundedPointer pointers[204]; // 204 * 20 = 4080
		public int64 padding; // 8B

	}  // 4080 + 8 + 8 = 4096

	public class Node {
		public int32 file_position = -1;
		public NodeContent content;

		BoundingBox calculated_bounding_box;
		bool recalculate_bounding_box = true;

		DiskNodeHandler disk_node_handler;

		public Node.root (DiskNodeHandler disk_node_handler) throws Error {
			this.disk_node_handler = disk_node_handler;
			initialize_root ();
			disk_node_handler.write_new (this);
		}

		public Node.copy_empty (Node node_to_copy, DiskNodeHandler disk_node_handler) throws Error {
			this.disk_node_handler = disk_node_handler;
			content.parent = node_to_copy.content.parent;
			content.type = node_to_copy.content.type;
			content.size = 0;
		}

		public Node.copy_content (Node node_to_copy, DiskNodeHandler disk_node_handler) throws Error {
			this.disk_node_handler = disk_node_handler;
			content.parent = node_to_copy.content.parent;
			content.type = node_to_copy.content.type;
			content.size = 0;
			for (int i=0; i<node_to_copy.content.size; i++) {
				add_element (node_to_copy.content.pointers[i]);
			}
		}

		public Node.from_position (int32 file_position, DiskNodeHandler disk_node_handler) throws Error {
			this.disk_node_handler = disk_node_handler;
			this.file_position = file_position;
			disk_node_handler.read_node_content (this);
		}

		public int max_pointer_length () {
			return content.pointers.length;
		}

		public int min_pointer_length () {
			return content.pointers.length/4;
		}

		void initialize_root () {
			file_position = 0;
			content.parent = -1;
			content.size = 0;
			content.type = NodeType.LEAF;

			assert (is_leaf ());
			assert (is_root ());
		}

		public bool is_leaf () {
			return content.type == NodeType.LEAF;
		}

		public bool is_root () {
			return file_position == 0;
		}

		public bool is_full () {
			return content.size >= max_pointer_length ();
		}

		BoundedPointer get_pointer () {
			return BoundedPointer (get_bounding_box (), file_position);
		}

		void add_element (BoundedPointer pointer) {
			calculated_bounding_box.extend (pointer.get_bounding_box ());
			content.pointers[content.size] = pointer;
			content.size++;
		}

		Node get_parent () throws Error {
			return disk_node_handler.get_node_at_position (content.parent);
		}

		public void insert_into_leaf (BoundedPointer pointer) throws Error {
			//print ("\n**INSERT**\n");
			Node node = choose_leaf (pointer.get_bounding_box ());
			assert (node.is_leaf ());
			node.add (pointer);
		}

		void add (BoundedPointer pointer) throws Error {
			//print (@"Inserting: $(pointer) into $(file_position)\n");
			if (is_full ()) {
				Node partner_node = split_node (pointer);
				adjust_tree_after_split (partner_node);
			} else {
				add_element (pointer);
				//print (@"Adding $(pointer.position) with: $(pointer.get_bounding_box ())\n");
				//print (@"Current BBOX: $(get_bounding_box ())\n");
				adjust_tree ();
			}
		}

		void adjust_tree () throws Error {
			if (!is_root ()) {
				Node parent = get_parent ();
				parent.set_pointer_bounds (file_position, get_bounding_box ());
				parent.adjust_tree ();
			}
			disk_node_handler.write (this);
		}

		void adjust_tree_after_split (Node new_node) throws Error {
			if (!is_root ()) {
				disk_node_handler.write_new (new_node);
				adjust_tree ();
				this.synchronize_parent ();
				new_node.synchronize_parent ();
				BoundedPointer new_node_pointer = new_node.get_pointer ();
				get_parent ().add (new_node_pointer);
			} else {
				Node new_node_2 = new Node.copy_content (this, disk_node_handler);

				content.type = NodeType.TREE;
				content.size = 0;

				recalculate_bounding_box = true;

				new_node.content.parent = file_position;
				new_node_2.content.parent = file_position;

				disk_node_handler.write_new (new_node);
				new_node.synchronize_parent ();
				add_element (new_node.get_pointer ());

				disk_node_handler.write_new (new_node_2);
				new_node_2.synchronize_parent ();
				add_element (new_node_2.get_pointer ());

				disk_node_handler.write (this);

				//check_integrity_after_split (new_node, new_node_2);
			}
		}

		void check_integrity_after_split (Node node1, Node node2) {
			assert (content.size == 2);

			if (content.pointers[0].position != node1.file_position) {
				print (@"$(content.pointers[0].position) $(node1.file_position)\n");
			}

			assert (content.pointers[0].position == node1.file_position);
			assert (content.pointers[1].position == node2.file_position);

			if (node1.content.parent != file_position) {
				print (@"$(node1.content.parent) $(file_position)\n");
			}
			assert (node1.content.parent == file_position);
			assert (node1.content.size >= min_pointer_length ());

			assert (node2.content.parent == file_position);
			assert (node2.content.size >= min_pointer_length ());
		}

		void set_pointer_bounds (int32 node_position, BoundingBox bbox) {
			for (int i=0; i<content.size; i++) {
				if (content.pointers[i].position == node_position) {
					BoundedPointer pointer = content.pointers[i];
					pointer.set_bounding_box (bbox);
					content.pointers[i] = pointer;
					//print (@"Check: $(content.pointers[i].get_bounding_box ()) - $(bbox)\n");

					calculated_bounding_box.extend (bbox);
					return;
				}
			}
			print ("Size: %d\n", content.size);
			for (int i=0; i<content.size; i++) {
				print ("Pointer %s\n", content.pointers[i].position.to_string ());
			}
			error ("Requested pointer %s on %s not found!", node_position.to_string (), file_position.to_string ());
		}

		public Node split_node (BoundedPointer input_pointer) throws Error {
			int low_index;
			int high_index;
			pick_index_seeds (out low_index, out high_index);

			Node new_node = new Node.copy_empty (this, disk_node_handler);

			//print ("Splitting: %s\n", file_position.to_string ());
			BoundedPointer[] old_pointers = content.pointers;

			recalculate_bounding_box = true;
			content.size = 0;

			this.add_element (old_pointers[low_index]);
			new_node.add_element (old_pointers[high_index]);

			BoundedPointer each_pointer;

			for (int i=0; i<old_pointers.length; i++) {
				if (i == low_index || i == high_index) {
					continue;
				}
				each_pointer = old_pointers[i];
				if (content.size < min_pointer_length ()) {
					this.add_element (each_pointer);
				} else if (new_node.content.size < min_pointer_length ()) {
					new_node.add_element (each_pointer);
				} else {
					add_to_best_node (this, new_node, each_pointer);
				}
			}

			add_to_best_node (this, new_node, input_pointer);

			if (this.content.size + new_node.content.size != old_pointers.length + 1) {
				error ("%d + %d != %d + 1 - Sizes of nodes after split do not match expected sizes!", this.content.size, new_node.content.size, old_pointers.length);
			}

			return new_node;
		}

		void add_to_best_node (Node node1, Node node2, BoundedPointer pointer_to_add) {
			double area1 = node1.get_bounding_box ().area_size_with (pointer_to_add.get_bounding_box ());
			double area2 = node2.get_bounding_box ().area_size_with (pointer_to_add.get_bounding_box ());

			if (area1 < area2) {
				node1.add_element (pointer_to_add);
			} else if (area1 > area2) {
				node2.add_element (pointer_to_add);
			} else {
				// TODO: Decide on node size istead.
				node2.add_element (pointer_to_add);
			}
		}

		void pick_index_seeds (out int low_index, out int high_index) {
			double separation = calculate_separation();

			BoundingBox bounding_box = get_bounding_box ();
			double normalized_height = separation / bounding_box.height ();
			double normalized_width = separation / bounding_box.width ();

			BoundingBox each_bounding_box = content.pointers[0].get_bounding_box ();
			double each_separation = Math.fabs (each_bounding_box.south - normalized_height) + Math.fabs (each_bounding_box.west - normalized_width);
			low_index = 0;
			high_index = 0;
			double low_separation = each_separation;
			double high_separation = each_separation;

			for (int i=1; i<content.size; i++) {
				each_bounding_box = content.pointers[i].get_bounding_box ();
				each_separation = Math.fabs (each_bounding_box.south - normalized_height) + Math.fabs (each_bounding_box.west - normalized_width);
				if (each_separation < low_separation) {
					low_separation = each_separation;
					low_index = i;
				} else if (each_separation > high_separation) {
					high_separation = each_separation;
					high_index = i;
				}
			}
		}

		double calculate_separation () {
			BoundingBox bounding_box = content.pointers[0].get_bounding_box ();
			Point lowest = bounding_box.low ();
			Point highest = bounding_box.high ();

			for (int i=1; i<content.size; i++) {
				bounding_box = content.pointers[i].get_bounding_box ();
				if (bounding_box.low ().compare_to (lowest) < 0) {
					lowest = bounding_box.low ();
				}

				if (bounding_box.high ().compare_to (highest) > 0) {
					highest = bounding_box.high ();
				}
			}
			return lowest.distance (highest);
		}

		BoundingBox? get_bounding_box () {
			if (content.size <= 0) {
				return null;
			}
			if (recalculate_bounding_box) {
				calculated_bounding_box = content.pointers[0].get_bounding_box ();
				for (int i=1; i<content.size; i++) {
					calculated_bounding_box.extend (content.pointers[i].get_bounding_box ());
				}
				recalculate_bounding_box = false;
			}
			return calculated_bounding_box;
		}

		public Node? choose_leaf (BoundingBox bbox) throws Error {
			if (is_leaf ()) {
				return this;
			} else {
				var pointer = get_pointer_with_smallest_area (bbox);
				var sub_node = disk_node_handler.get_node_at_position (pointer.position);
				return sub_node.choose_leaf (bbox);
			}
		}

		BoundedPointer? get_pointer_with_smallest_area (BoundingBox bbox) {
			if (content.size <= 0) {
				error ("Call get pointer on empty node!");
			}

			BoundedPointer best_pointer = content.pointers[0];
			double best_area = best_pointer.get_bounding_box ().area_size_with (bbox);

			for (int i=1; i<content.size; i++) {
				double area = content.pointers[i].get_bounding_box ().area_size_with (bbox);
				if (area < best_area) {
					best_pointer = content.pointers[i];
					best_area = area;
				}
			}
			return best_pointer;
		}

		public void synchronize_parent () throws Error {
			if (is_leaf ()) {
				return;
			}
			for (int i=0; i<content.size; i++) {
				Node node = disk_node_handler.get_node_at_position (content.pointers[i].position);
				node.content.parent = file_position;
				disk_node_handler.write (node);
			}
		}

		public NodePointerIterator pointer_iterator () {
			return new NodePointerIterator (this);
		}


		public class NodePointerIterator {
			int index = -1;
			Node node;

			public NodePointerIterator (Node node) {
				this.node = node;
			}

			public BoundedPointer get () {
				return node.content.pointers[index];
			}

			public bool next () {
				index++;
				if (index >= node.content.size) {
					return false;
				}
				return true;
			}

			public NodePointerIterator iterator () {
				return this;
			}
		}
	}
}
