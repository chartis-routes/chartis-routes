
namespace Spatial {
	public class DiskNodeHandler {
		Gee.Map<int32?, Node> cache = new Gee.HashMap<int32?, Node> (int_hash, int_equal, direct_equal);

		InputStream input_stream;
		OutputStream output_stream;

		public DiskNodeHandler.from_file (File file, bool compress = true) throws Error {
			FileIOStream io_stream;
			bool is_new = false;
			if (file.query_exists ()) {
				io_stream = file.open_readwrite ();
				is_new = false;
			} else {
				io_stream = file.create_readwrite (FileCreateFlags.REPLACE_DESTINATION);
				is_new = true;
			}

			input_stream = io_stream.get_input_stream ();
			output_stream = io_stream.get_output_stream ();

			if (input_stream is Seekable == false) {
				throw new IOError.FAILED ("Seekable not supported!");
			}

			if (output_stream is Seekable == false) {
				throw new IOError.FAILED ("Seekable not supported!");
			}

			if (is_new) {
				new Node.root (this);
			}
		}

		Seekable get_input_seekable () {
			return input_stream as Seekable;
		}

		Seekable get_output_seekable () {
			return output_stream as Seekable;
		}

		public Node get_node_at_position (int32 position) throws Error {
			if (!cache.has_key (position)) {
				Node node = new Node.from_position (position, this);
				cache.set (position, node);
				return node;
			}
			return cache.get (position);
		}

		public void write_new (Node node) throws Error {
			get_output_seekable ().seek (0, SeekType.END);
			node.file_position = (int32) get_output_seekable ().tell ();
			write_only (node);
			cache.set (node.file_position, node);
		}

		public void write (Node node) throws Error {
			get_output_seekable ().seek (node.file_position, SeekType.SET);
			write_only (node);
		}

		void write_only (Node node) throws Error {
			NodeContent* content_pointer = &node.content;
			unowned uint8[] buffer = (uint8[]) content_pointer;
			buffer.length = (int) sizeof (NodeContent);

			size_t written;
			bool result = output_stream.write_all (buffer, out written);
			if (result == false) {
				error ("Error at write_all - node not written!");
			}
		}

		public void read_node_content (Node node) throws Error {
			get_input_seekable ().seek (node.file_position, SeekType.SET);

			uint8[] node_content_byte_array = new uint8[sizeof (NodeContent)];
			size_t read;
			bool result = input_stream.read_all (node_content_byte_array, out read);
			if (!result || read == 0) {
				error ("Could not read!");
			}
			NodeContent* node_content = (NodeContent*) node_content_byte_array;
			node.content = *node_content;
			/*print (@"Loaded $(node.content.size) $(node.file_position)\n");
			for (int i=0; i<node.content.size; i++) {
				var pointer = node.content.pointers[i];
				print (@"Node $(pointer.get_bounding_box ()) - $(pointer.position)\n");
			}*/
		}

		public void close () throws Error {
			input_stream.close ();
			output_stream.close ();
		}
	}
}
