
public int main(string[] args) {
	Test.init (ref args);

	Test.add_func ("/distance/simple", GeoToolsTest.test_distance_simple);
	Test.add_func ("/distance/exact", GeoToolsTest.test_distance_exact);

	Test.add_func ("/BoundingBox/test_create_instance", TestBoundingBox.test_create_instance);
	Test.add_func ("/BoundingBox/test_includes", TestBoundingBox.test_includes);
	Test.add_func ("/BoundingBox/test_intersection", TestBoundingBox.test_intersection);
	Test.add_func ("/BoundingBox/test_merge", TestBoundingBox.test_merge);
	Test.add_func ("/BoundingBox/test_merge_check_copy", TestBoundingBox.test_merge_check_copy);
	Test.add_func ("/BoundingBox/test_area", TestBoundingBox.test_area);

	TestRTreeDisk.add_tests ();

	Test.add_func ("/Cache/collect_garbage", CacheTest.test_cache_collect_garbage);

	//NetworkTest.add_tests ();

	Test.run ();
	return 0;
}
