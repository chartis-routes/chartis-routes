valac-0.14 \
	--pkg libxml-2.0 \
	--pkg gee-1.0 \
	--pkg gtk+-3.0 \
	-o chartis-measure \
	chartis-measure.vala \
	base/base-structures.vala \
	base/geo-tools.vala \
	gpx/gpx-parser-structure.vala \
	gpx/gpx-parser.vala \
	gpx/xml-helper.vala \
