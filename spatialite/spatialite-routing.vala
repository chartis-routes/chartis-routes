
public class SpatialiteRouting : Object {
	Sqlite.Database database;

	construct {
		Sqlite.Spatialite.init(0);
		int result = Sqlite.Database.open ("slovenia.sqlite", out database);
		if (result == 1) {
			printerr ("Can't open database: %s\n", database.errmsg ());
		}
	}

	public int get_node_for_street_name (string street_name) {
		int n_row;
		int n_column;
		int result = 0;
		string[] results;

		string sql = "SELECT Node_To FROM Roads WHERE name like \"%s%%\"".printf(street_name);
		result = database.get_table (sql, out results, out n_row, out n_column);
		print ("Search \"%s\", %d, %s %s %s %s\n", street_name, n_row, results[0], results[1], results[2], results[3]);

		return results[1].to_int ();
	}

	public int get_nearest_node (double latitude, double longitude) {
		int n_row;
		int n_column;
		int result = 0;
		string[] results;

		string sql = "SELECT Node_To, Min(Distance(geometry, MakePoint(%.8f, %.8f))) FROM roads".printf (latitude, longitude);
		result = database.get_table (sql, out results, out n_row, out n_column);
		print ("SQL: %s\n", sql);
		print ("Nearest: %s Dist: %s Rows: %d Columns: %d\n", results[2], results[3], n_row, n_column);

		return results[2].to_int ();
	}

	public void get_route (int64 from, int64 to) {
		int rows;
		int columns;
		int query_result = 0;
		string[] results;

		print ("%s %s\n", from.to_string (), to.to_string ());

		string sql = "SELECT NumPoints(Geometry) FROM Roads_net WHERE nodeFrom = %s AND nodeTo = %s LIMIT 1".printf (from.to_string (), to.to_string ());
		string error_message;
		query_result = database.get_table (sql, out results, out rows, out columns, out error_message);

		print ("Rows: %d Columns: %d\n", rows, columns);

		if (query_result != Sqlite.OK) {
			print ("SQL error: %s\n", error_message);
			return;
		}

		int points = results[0].to_int ();

		for (int i = 1; i <= points; i++) {
			sql = "SELECT X(PointN(Geometry, %d)), Y(PointN(Geometry, %d)) FROM Roads_net WHERE nodeFrom = %s and nodeTo = %s LIMIT 1".printf (i, i, from.to_string (), to.to_string ());
			query_result = database.get_table (sql, out results, out rows, out columns);
			double lon = results[2].to_double ();
			double lat = results[3].to_double ();
			print ("%f %f\n", lon, lat);
		}
	}
}
