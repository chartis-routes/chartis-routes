#!/bin/sh

echo "Creating spatialite file '$2' from openstreetmap data file '$1'"
spatialite_osm_net -o $1 -d $2 -T roads -m
echo "Populating road network data"
spatialite_network -d $2 -T roads -g geometry -c cost -t node_to -f node_from -n name --oneway-fromto oneway_fromto --oneway-tofrom oneway_tofrom -o roads_net_data --overwrite-output
echo "Creating virtual table 'roads_net' for routing queries."
spatialite $2 'CREATE VIRTUAL TABLE "roads_net" USING VirtualNetwork("roads_net_data")'
echo "Done!"

