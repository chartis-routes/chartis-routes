public class RouteConnection {
	public double cost;
	public int8 grade;
	public Node source;
	public Node destination;
	public RouteConnection previous_connection;

	public double distance () throws Error {
		return GeoTools.distance_simple (source.get_location (), destination.get_location ());
	}

	public double total_cost_short () {
		return cost;
	}

	public double total_cost_fast () {
		return cost / (grade / 10.0);
	}
}

public interface RouteFinder : Object {
	public abstract void find (Node source, Node destination) throws Error;
	public abstract unowned Gee.List<Node> get_solution ();
	public abstract double get_solution_distance ();
	public abstract Gee.Set<Node> get_visited_nodes ();
}
