
public class TwoPointRouteFinderFast : Object, RouteFinder {
	protected Node source;
	protected Node target;

	Gee.List<Node> solution;
	double solution_distance = 0.0;

	PartialRouteFinder source_finder;
	PartialRouteFinder target_finder;

	public unowned Gee.List<Node> get_solution () {
		return solution;
	}

	public double get_solution_distance () {
		return solution_distance;
	}

	public static CompareFunc<RouteConnection> compare_function () {
		return (a,b) => {
			if (a.total_cost_fast () > b.total_cost_fast ()) {
				return 1;
			}
			return -1;
		};
	}

	void initialize_iteration (Node source, Node target) throws Error  {
		this.source = source;
		this.target = target;

		source_finder = new PartialRouteFinder (source);
		target_finder = new PartialRouteFinder (target);

		source_finder.check_for_solution = (connection) => {
			return target_finder.has_visited_node (connection.destination.get_id ());
		};

		target_finder.check_for_solution = (connection) => {
			return source_finder.has_visited_node (connection.destination.get_id ());
		};

		target_finder.reverse_finding = true;

		source_finder.fill_candidates (source, null);
		target_finder.fill_candidates (target, null);

		solution = new Gee.ArrayList<Node> ();
	}

	private class PartialRouteFinder : Object {
		public delegate bool SolutionCheck (RouteConnection connection);

		Gee.PriorityQueue<RouteConnection> candidates;
		Gee.HashMap<int64?, RouteConnection> visited_nodes;

		public Node search_source_node { get ; private set; }

		public SolutionCheck check_for_solution { get; set; }
		public bool reverse_finding { get; set; }

		public PartialRouteFinder (Node search_source_node) {
			this.search_source_node = search_source_node;
			candidates = new Gee.PriorityQueue<RouteConnection> (compare_function ());
			visited_nodes = new Gee.HashMap<int64?, RouteConnection> (int64_hash, int64_equal);
		}

		public void fill_solution (int64 node_id, Gee.List<Node> solution, out double solution_distance) {
			RouteConnection current = visited_nodes.get (node_id);

			solution.add (current.destination);
			while (current.source.get_id () != search_source_node.get_id ()) {
				if (!reverse_finding) {
					solution.insert (0, current.source);
				} else {
					solution.add (current.source);
				}
				current = current.previous_connection;
			}
		}

		public void fill_candidates (Node node, RouteConnection? previous) throws Error {
			double previous_cost = (previous == null ? 0.0 : previous.cost);

			foreach (Connection connection in node.get_connections ()) {
				check_connection (node, connection, previous, previous_cost);
			}
		}

		void check_connection (Node node, Connection connection, RouteConnection? previous, double previous_cost) throws Error {
			double cost;

			Node? other_node = navigate_connection (node, connection, out cost);

			if (other_node == null || visited_nodes.contains (other_node.get_id ())) {
				return;
			}

			var candidate = new RouteConnection ();
			candidate.source = node;
			candidate.destination = other_node;
			candidate.previous_connection = previous;
			candidate.grade = connection.get_grade ();
			candidate.cost = cost + previous_cost;
			candidates.add (candidate);
		}

		Node? navigate_connection (Node node, Connection connection, out double cost) throws Error {
			Node other_node = connection.get_other_end (node);
			if (other_node == null) {
				error ("Other node not found!");
			}

			if (reverse_finding) {
				if (!connection.is_connectable (node)) {
					return null;
				}
			} else {
				if (!connection.is_connectable (other_node)) {
					return null;
				}
			}

			cost = connection.get_distance ();
			return other_node;
		}

		Node? navigate_connection_extended (Node node, Connection connection, out double cost) throws Error {
			Node current_node = node;
			Connection current_connection = connection;
			Node? other_node;
			cost = 0.0;

			do {
				other_node = current_connection.get_other_end (current_node);
				if (!current_connection.is_connectable (other_node)) {
					return null;
				}
				cost += current_connection.get_distance ();

				var connections = other_node.get_connections ();
				if (connections.size == 2) {
					if (connections.first ().connects_with (current_node)) {
						current_connection = other_node.get_connections ().last ();
					} else {
						current_connection = other_node.get_connections ().first ();
					}
					current_node = other_node;
				} else {
					return other_node;
				}
			} while (true);
		}

		public int64 iterate () throws Error {
			RouteConnection? candidate = candidates.poll ();
			if (candidate == null) {
				error ("Empty candidate");
			}
			int64 node_id = candidate.destination.get_id ();
			visited_nodes.set (node_id, candidate);
			if (check_for_solution (candidate)) {
				return node_id;
			}
			fill_candidates (candidate.destination, candidate);

			return 0;
		}

		public bool has_visited_node (int64 node_id) {
			return visited_nodes.contains (node_id);
		}

		public void clear () {
			candidates.clear ();
			visited_nodes.clear ();
		}
	}

	public virtual void find (Node source, Node target) throws Error {
		initialize_iteration (source, target);

		int64 solution_node_id = 0;

		int iteration = 0;
		do {
			iteration++;

			if (iteration % 1000 == 0) {
				print ("Iteration: %d\r", iteration);
			}

			if (iteration % 2 == 0) {
				solution_node_id = source_finder.iterate ();
			} else {
				solution_node_id = target_finder.iterate ();
			}
		} while (solution_node_id == 0);

		fill_solution (solution_node_id);

		source_finder.clear ();
		target_finder.clear ();
	}

	void fill_solution (int64 node_id) throws Error {
		solution_distance = 0.0;

		source_finder.fill_solution (node_id, solution, out solution_distance);
		target_finder.fill_solution (node_id, solution, out solution_distance);

		solution.insert (0, source);
		solution.add (target);
	}

	public Gee.Set<RouteConnection> get_visited_nodes () {
		return new Gee.HashSet<RouteConnection> ();
	}
}
