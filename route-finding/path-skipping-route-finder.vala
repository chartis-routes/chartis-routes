
public class PathSkippingRouteFinder : BaseRouteFinderShort {

	protected override Node? go_to_other (Node node, Connection connection, out double cost) throws Error {
		Node current_node = node;
		Connection current_connection = connection;
		Node? other_node;
		cost = 0.0;

		do {
			other_node = current_connection.get_other_end (current_node);
			if (!current_connection.is_connectable (other_node)) {
				return null;
			}
			cost += current_connection.get_distance ();

			if (other_node.get_id () == destination.get_id ()) {
				return other_node;
			}

			var connections = other_node.get_connections ();
			if (connections.size == 2) {
				if (connections.first ().connects_with (current_node)) {
					current_connection = other_node.get_connections ().last ();
				} else {
					current_connection = other_node.get_connections ().first ();
				}
				current_node = other_node;
			} else {
				return other_node;
			}
		} while (true);
	}
}
