
public class BaseRouteFinderFast : Object, RouteFinder {
	protected Node source;
	protected Node destination;

	protected Gee.PriorityQueue<RouteConnection> candidates;
	protected Gee.Map<int64?, RouteConnection> visited_nodes;

	Gee.List<Node> solution;
	double solution_distance = 0.0;

	public unowned Gee.List<Node> get_solution () {
		return solution;
	}

	public double get_solution_distance () {
		return solution_distance;
	}

	public Gee.Set<Node> get_visited_nodes () {
		Gee.Set<Node> visited = new Gee.HashSet<Node> ();
		foreach (var element in visited_nodes.entries) {
			visited.add (element.value.destination);
		}
		return visited;
	}

	protected virtual void initialize_iteration () throws Error  {
		initialize_priority_queue ();
		visited_nodes = new Gee.HashMap<int64?, RouteConnection> (int64_hash, int64_equal);
		solution = new Gee.ArrayList<Node> ();
		fill_candidates_for (source, null);
	}

	public static CompareFunc<RouteConnection> compare_function () {
		return (a,b) => {
			if (a.total_cost_fast () > b.total_cost_fast ()) {
				return 1;
			}
			return -1;
		};
	}

	protected virtual void initialize_priority_queue () throws Error  {
		candidates = new Gee.PriorityQueue<RouteConnection> (compare_function ());
	}

	protected virtual Node? go_to_other (Node node, Connection connection, out double cost) throws Error {
		Node other_node = connection.get_other_end (node);
		if (other_node == null) {
			error ("Other node not found!");
		}

		if (!connection.is_connectable (other_node)) {
			return null;
		}

		cost = connection.get_distance ();
		return other_node;
	}

	protected virtual void fill_candidates_for (Node node, RouteConnection? previous) throws Error {
		double previous_cost = (previous == null ? 0.0 : previous.cost);

		foreach (Connection connection in node.get_connections ()) {
			check_connection (node, connection, previous, previous_cost);
		}
	}

	void check_connection (Node node, Connection connection, RouteConnection? previous_connection, double previous_cost) throws Error {
		double cost;

		Node? other_node = go_to_other (node, connection, out cost);

		if (other_node == null) {
			return;
		}

		if (visited_nodes.contains (other_node.get_id ())) {
			return;
		}

		var candidate = new RouteConnection ();
		candidate.source = node;
		candidate.destination = other_node;
		candidate.previous_connection = previous_connection;
		candidate.grade = connection.get_grade ();
		candidate.cost = cost + previous_cost;
		candidates.add (candidate);
	}

	public virtual void find (Node source, Node destination) throws Error {
		this.source = source;
		this.destination = destination;
		initialize_iteration ();

		bool solution_fund = false;
		RouteConnection? candidate;
		int iteration = 0;
		while (!solution_fund) {
			iteration++;
			if (iteration % 1000 == 0) {
				print ("Iteration: %d\r", iteration);
			}
			candidate = candidates.poll ();
			if (candidate == null) {
				solution_fund = true;
			} else if (candidate.destination.get_id () == destination.get_id ()) {
				solution_fund = true;
				fill_solution (candidate);
			} else {
				visited_nodes.set (candidate.destination.get_id (), candidate);
				fill_candidates_for (candidate.destination, candidate);
			}
		}
		candidates.clear ();
	}

	void fill_solution (RouteConnection connection) throws Error {
		RouteConnection current = connection;

		solution.add (destination);
		while (current.source.get_id () != source.get_id ()) {
			solution.insert (0, current.source);
			current = current.previous_connection;
		}
		solution.insert (0, source);
	}
}
