

public class DistanceBasedPathSkippingRouteFinder : PathSkippingRouteFinder {
	double base_distance;

	protected override void initialize_iteration () throws Error {
		base.initialize_iteration ();
		base_distance = GeoTools.distance_simple (source.get_location (), destination.get_location ());
	}

	public static CompareFunc<RouteConnection> compare_function () {
		return (a,b) => {
			if (a.total_cost_short () > b.total_cost_short ()) {
				return 1;
			}
			return -1;
		};
	}

	protected override void initialize_priority_queue () throws Error  {
		candidates = new Gee.PriorityQueue<RouteConnection> (compare_function ());
	}

	protected RouteConnection? find_best_candidate () throws Error {
		RouteConnection? best = null;
		double best_cost = double.MAX;
		foreach (RouteConnection candidate in candidates) {
			double candidate_distance = GeoTools.distance_simple (candidate.destination.get_location (), destination.get_location ());
			double distance_factor = (candidate_distance / base_distance);
			double grade_factor = (candidate.grade / 10.0);

			var candidate_total_cost = candidate.cost * distance_factor * grade_factor;

			if (best == null || candidate_total_cost < best_cost) {
				best = candidate;
				best_cost = candidate_total_cost;
			}
		}
		return best;
	}
}
