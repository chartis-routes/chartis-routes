
public class TwoPointRouteFinderPreserveSolution : Object, RouteFinder {
	protected Node source;
	protected Node target;

	protected Gee.PriorityQueue<RouteConnection> candidates_source;
	protected Gee.PriorityQueue<RouteConnection> candidates_target;

	protected Gee.Map<int64?, RouteConnection> visited_nodes_source;
	protected Gee.Map<int64?, RouteConnection> visited_nodes_target;

	Gee.List<Node> solution;
	double solution_distance = 0.0;

	public unowned Gee.List<Node> get_solution () {
		return solution;
	}

	public double get_solution_distance () {
		return solution_distance;
	}

	public Gee.Set<Node> get_visited_nodes () {
		Gee.Set<Node> visited = new Gee.HashSet<Node> ();
		foreach (var element in visited_nodes_source.entries) {
			visited.add (element.value.destination);
		}
		foreach (var element in visited_nodes_target.entries) {
			visited.add (element.value.destination);
		}
		return visited;
	}

	void initialize_iteration (Node source, Node target) throws Error  {
		this.source = source;
		this.target = target;

		candidates_source = new Gee.PriorityQueue<RouteConnection> (RouteConnection.compare_function ());
		candidates_target = new Gee.PriorityQueue<RouteConnection> (RouteConnection.compare_function ());

		visited_nodes_source = new Gee.HashMap<int64?, RouteConnection> (int64_hash, int64_equal);
		visited_nodes_target = new Gee.HashMap<int64?, RouteConnection> (int64_hash, int64_equal);

		solution = new Gee.ArrayList<Node> ();
	}

	Node? navigate_connection (Node node, Connection connection, out double cost, out double total_distance) throws Error {
		Node other_node = connection.get_other_end (node);
		if (other_node == null) {
			error ("Other node not found!");
		}

		if (!connection.is_connectable (other_node)) {
			return null;
		}

		cost = connection.get_distance ();
		total_distance = connection.get_distance ();
		return other_node;
	}

	void fill_candidates_source (Node node, RouteConnection? previous) throws Error {
		fill_candidates (node, previous, candidates_source, visited_nodes_source);
	}

	void fill_candidates_target (Node node, RouteConnection? previous) throws Error {
		fill_candidates (node, previous, candidates_target, visited_nodes_target);
	}

	void fill_candidates (Node node, RouteConnection? previous, Gee.PriorityQueue<RouteConnection> candidates, Gee.Map<int64?, RouteConnection> visited_nodes) throws Error {
		double previous_cost = (previous == null ? 0.0 : previous.cost);
		double previous_total_distance = (previous == null ? 0.0 : previous.total_distance);

		foreach (Connection connection in node.get_connections ()) {
			check_connection (node, connection, previous, previous_cost, previous_total_distance, candidates, visited_nodes);
		}
	}

	void check_connection (Node node, Connection connection, RouteConnection? previous, double previous_cost, double previous_total_distance, Gee.PriorityQueue<RouteConnection> candidates, Gee.Map<int64?, RouteConnection> visited_nodes) throws Error {
		double cost;
		double total_distance;

		Node? other_node = navigate_connection (node, connection, out cost, out total_distance);

		if (other_node == null) {
			return;
		}

		if (visited_nodes.contains (other_node.get_id ())) {
			return;
		}

		var candidate = new RouteConnection ();
		candidate.source = node;
		candidate.destination = other_node;
		candidate.previous_connection = previous;
		candidate.grade = connection.get_grade ();
		candidate.cost = cost + previous_cost;
		candidate.total_distance = total_distance + previous_total_distance;
		candidates.add (candidate);
	}

	RouteConnection? find_best_candidate (Gee.PriorityQueue<RouteConnection> candidates) throws Error {
		return candidates.poll ();
	}

	public virtual void find (Node source, Node target) throws Error {
		initialize_iteration (source, target);

		fill_candidates_source (source, null);
		fill_candidates_target (target, null);

		bool solution_found = false;

		int iteration = 0;
		do {
			iteration++;
			if (iteration % 1000 == 0) {
				print ("Iteration: %d\r", iteration);
			}

			if (iteration % 2 == 0) {
				solution_found = iterate_source ();
			} else {
				solution_found = iterate_target ();
			}
		} while (!solution_found);
		candidates_source.clear ();
		candidates_target.clear ();
	}

	bool has_solution_been_found_for_source (RouteConnection candidate) throws Error {
		return visited_nodes_target.contains (candidate.destination.get_id ());
	}

	bool has_solution_been_found_for_target (RouteConnection candidate) throws Error {
		return visited_nodes_source.contains (candidate.destination.get_id ());
	}

	bool iterate_source () throws Error {
		RouteConnection? candidate = find_best_candidate (candidates_source);
		if (candidate == null) {
			error ("Empty candidate");
		}

		visited_nodes_source.set (candidate.destination.get_id (), candidate);

		if (has_solution_been_found_for_source (candidate)) {
			fill_solution (candidate.destination.get_id ());
			return true;
		}

		fill_candidates_source (candidate.destination, candidate);
		return false;
	}

	bool iterate_target () throws Error {
		RouteConnection? candidate = find_best_candidate (candidates_target);
		if (candidate == null) {
			error ("Empty candidate");
		}

		visited_nodes_target.set (candidate.destination.get_id (), candidate);

		if (has_solution_been_found_for_target (candidate)) {
			fill_solution (candidate.destination.get_id ());
			return true;
		}

		candidates_target.remove (candidate);
		fill_candidates_target (candidate.destination, candidate);
		return false;
	}

	void fill_solution (int64 node_id) throws Error {
		RouteConnection current;

		current = visited_nodes_source.get (node_id);
		solution_distance = current.total_distance;
		solution.add (current.destination);
		while (current.source.get_id () != source.get_id ()) {
			solution.insert (0, current.source);
			current = current.previous_connection;
		}
		solution.insert (0, source);

		current = visited_nodes_target.get (node_id);
		solution_distance += current.total_distance;
		while (current.source.get_id () != target.get_id ()) {
			solution.add (current.source);
			current = current.previous_connection;
		}
		solution.add (target);
	}


}
