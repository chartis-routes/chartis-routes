public static int main (string[] args) {
	string filename = null;
	string type = null;

	OptionEntry entry1 = {"file", 0, OptionFlags.IN_MAIN, OptionArg.STRING, ref filename, null, "Filename"};
	OptionEntry entry2 = {"type", 0, OptionFlags.IN_MAIN, OptionArg.STRING, ref type, null, "Type"};
	OptionEntry[] options = { entry1, entry2 };

	var opt_context = new OptionContext ("");
	opt_context.set_help_enabled (true);
	opt_context.add_main_entries (options, null);
	try {
		opt_context.parse (ref args);
	} catch (OptionError er) {
		print (er.message);
	}

	if (type == "csv") {
		measure_csv (filename);
	} else if (type == "gpx") {
		measure_gpx (filename);
	}

	return 0;
}

public static void measure_gpx (string file_path) throws Error {
	var parser = new Gpx.Parser ();
	var root = parser.parse_file (file_path);

	double distance = 0.0;
	bool previous = false;
	double previous_lat = double.NAN;
	double previous_lon = double.NAN;

	foreach (Gpx.Track track in root.tracks) {
		foreach (Gpx.TrackSegment segment in track.segments) {
			foreach (Gpx.Waypoint point in segment.points) {
				if (previous) {
					distance += GeoTools.distance_exact_lat_lon (previous_lat, previous_lon, point.latitude, point.longitude);
				}
				previous_lat = point.latitude;
				previous_lon = point.longitude;
				previous = true;
			}
		}
	}
	print ("Distance: %0.4f\n", distance);
}

public static void measure_csv (string file_path) throws Error {
	var file_stream = FileStream.open (file_path, "r");

	double distance = 0.0;
	bool previous = false;
	double previous_lat = double.NAN;
	double previous_lon = double.NAN;

	for (string line = file_stream.read_line () ; line != null; line = file_stream.read_line ()) {
		var array_of_string = line.replace ("\"", "").replace(",", ";").replace(" ", ";").split (";");
		if (array_of_string.length < 2) {
			continue;
		}

		double latitude;
		if (!double.try_parse (array_of_string[0], out latitude)) {
			continue;
		}

		double longitude;
		if (!double.try_parse (array_of_string[1], out longitude)) {
			continue;
		}

		if (previous) {
			distance += GeoTools.distance_exact_lat_lon (previous_lat, previous_lon, latitude, longitude);
		}
		previous_lat = latitude;
		previous_lon = longitude;
		previous = true;
	}

	print ("Distance: %0.4f\n", distance);
}
