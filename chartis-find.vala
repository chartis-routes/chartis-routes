public class MainApplication : Object {
	DiskNetwork network;

	public MainApplication() {
		try {
			var routing_directory = File.new_for_path("routing-data");
			if (!routing_directory.query_exists ()) {
				print ("Routing data directory does not exists\n");
			}

			network = new DiskNetwork (routing_directory);
		} catch (Error e) {
			error (e.message);
		}
	}

	public void write_solution_to_file (RouteFinder finder) throws Error {
		var file = File.new_for_path("result.csv");
		if (file.query_exists ()) {
			file.delete ();
		}
		var io_stream = file.create_readwrite (FileCreateFlags.REPLACE_DESTINATION);
		var output_stream = io_stream.get_output_stream ();
		var data_output_stream = new DataOutputStream (output_stream);

		bool previous = false;
		double previous_lat = 0.0;
		double previous_lon = 0.0;
		double distance = 0.0;

		foreach (Node node in finder.get_solution ()) {
			double latitude = node.get_location ().latitude;
			double longitude = node.get_location ().longitude;

			data_output_stream.put_string ("%.6f;%.6f\n".printf (latitude, longitude));

			if (previous) {
				distance += GeoTools.distance_exact_lat_lon (previous_lat, previous_lon, latitude, longitude);
			}

			previous_lat = latitude;
			previous_lon = longitude;
			previous = true;
		}
		print ("Distance: %.4f\n", distance);
	}

	public RouteFinder? get_route_finder (int algorithm) {
		switch (algorithm) {
			case 1:
				print ("TwoPointRouteFinderShort\n");
				return new TwoPointRouteFinderShort ();
			case 2:
				print ("BaseRouteFinderShort\n");
				return new BaseRouteFinderShort ();
			case 3:
				print ("TwoPointRouteFinderFast\n");
				return new TwoPointRouteFinderFast ();
			case 4:
				print ("BaseRouteFinderFast\n");
				return new BaseRouteFinderFast ();
			case 5:
				print ("PathSkippingRouteFinder\n");
				return new PathSkippingRouteFinder ();
			case 6:
				print ("DistanceBasedRouteFinder\n");
				return new DistanceBasedRouteFinder ();
		}
		return null;
	}

	public void calculate_route (Point start, Point end, int algorithm) {
		try {
			Timer timer = new Timer ();

			Node? start_node = network.find_nearest_node (start);
			if (start_node == null) {
				print ("Nearest node could not be found.\n");
				return;
			}
			print ("Find nearest in start node: %.3f %s\n", timer.elapsed (), start_node.get_location ().to_string ());
			timer.reset ();

			Node? end_node = network.find_nearest_node  (end);
			if (end_node == null) {
				print ("Nearest node could not be found.\n");
				return;
			}
			print ("Find nearest in end node: %.3f %s\n", timer.elapsed (), end_node.get_location ().to_string ());

			RouteFinder? finder;
			finder = get_route_finder (algorithm);

			if (finder == null) {
				print ("No route finder found!\n");
				return;
			}
			timer.reset ();
			finder.find (start_node, end_node);
			print ("Route Finder '%d' found solution in %f.\n", algorithm, timer.elapsed ());
			write_solution_to_file (finder);
		} catch (Error e) {
			error (e.message);
		}
	}
}

public static int main (string[] args) {
	double source_latitude;
	double source_longitude;
	double target_latitude;
	double target_longitude;
	int search_algorithm = 1;

	OptionEntry entry1 = {"lat1", 0, OptionFlags.IN_MAIN, OptionArg.DOUBLE, ref source_latitude, null, "Source latitude"};
	OptionEntry entry2 = {"lon1", 0, OptionFlags.IN_MAIN, OptionArg.DOUBLE, ref source_longitude, null, "Source longitude"};
	OptionEntry entry3 = {"lat2", 0, OptionFlags.IN_MAIN, OptionArg.DOUBLE, ref target_latitude, null, "Target latitude"};
	OptionEntry entry4 = {"lon2", 0, OptionFlags.IN_MAIN, OptionArg.DOUBLE, ref target_longitude, null, "Target longitude"};
	OptionEntry entry5 = {"alg", 0, OptionFlags.IN_MAIN, OptionArg.INT, ref search_algorithm, null, "Search algorithm used"};
	OptionEntry[] options = { entry1, entry2, entry3, entry4, entry5 };

	var opt_context = new OptionContext ("");
	opt_context.set_help_enabled (true);
	opt_context.add_main_entries (options, null);
	try {
		opt_context.parse (ref args);
	} catch (OptionError er) {
		print (er.message);
	}

	var application = new MainApplication ();
	application.calculate_route ({source_latitude, source_longitude}, {target_latitude, target_longitude}, search_algorithm);
	return 0;
}
