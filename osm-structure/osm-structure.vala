public struct NodeContent {
	public int64 id;
	public float latitude;
	public float longitude;
	public uint16 number_of_connections;

	public Point get_location () {
		return {(double) latitude, (double) longitude};
	}

	public void set_location (Point point) {
		this.latitude = (float) point.latitude;
		this.longitude = (float) point.longitude;
	}

	public void set_latitude (double latitude) {
		this.latitude = (float) latitude;
	}

	public void set_longitude (double longitude) {
		this.longitude = (float) longitude;
	}

	public string to_string () {
		return @"NodeContent: $(id) {$(number_of_connections)} %.4f %.4f".printf (latitude, longitude);
	}
}

public struct ConnectionContent {
	public int64 node1;
	public int64 node2;
	public int64 flags;

	public ConnectionContent () {
		node1 = -1;
		node2 = -1;
		flags = 0;
	}
}

public interface Node : Object {
	public abstract void set_id (int64 id) throws Error;
	public abstract void set_location (Point location) throws Error;
	public abstract void set_location_latitude (double latitude) throws Error;
	public abstract void set_location_longitude (double longitude) throws Error;
	public abstract int64 get_id () throws Error;
	public abstract Point get_location () throws Error;
	public abstract void add_connection (Connection connection) throws Error;
	public abstract unowned Gee.List<Connection> get_connections () throws Error;
	public abstract string to_string () throws Error;
}

public interface Connection : Object {
	public abstract void set_source (Node node) throws Error;
	public abstract void set_destination (Node node) throws Error;
	public abstract Node get_source () throws Error;
	public abstract Node get_destination () throws Error;
	public abstract Node? get_other_end (Node node) throws Error;
	public abstract double get_distance () throws Error;
	public abstract bool connects_with (Node node) throws Error;
	public abstract bool is_connectable (Node node) throws Error;
	public abstract void set_grade (int8 grade) throws Error;
	public abstract int8 get_grade () throws Error;
}

public interface Way : Object {
	public abstract void set_id (int64 id) throws Error;
	public abstract void add_node_by_id (int64 node_id) throws Error;
	public abstract unowned Gee.List<int64?> get_node_ids () throws Error;
	public abstract void add_tag (string key, string value);
}

public interface Network : Object {
	public abstract Node? find_nearest_node (Point location) throws Error;
}
