public class ImportNetwork : Object {
	Spatial.RTreeDisk rtree;

	DiskReaderWriter disk_reader_writer;
	DiskReaderWriter temporary_disk_reader_writer;

	bool closed = false;
	bool use_temporary = false;

	Gee.Map<int64?, DiskNode> nodes_by_id = new Gee.HashMap<int64?, DiskNode> (int64_hash, int64_equal, direct_equal);
	Gee.List<DiskWay?> ways = new Gee.ArrayList<DiskWay?> ();

	public ImportNetwork (File directory) throws Error {
		assert (directory.query_exists ());
		if (directory.query_file_type (FileQueryInfoFlags.NONE) != FileType.DIRECTORY) {
			error ("Not an directory");
		}

		var index_file = File.new_for_path (directory.get_path () + "/" + "index.data");

		if (index_file.query_exists ()) {
			index_file.delete ();
		}

		rtree = new Spatial.RTreeDisk (index_file);
		rtree.info ();

		var nodes_file = File.new_for_path (directory.get_path () + "/" + "nodes.data");

		if (nodes_file.query_exists ()) {
			nodes_file.delete ();
		}

		//var temporary_nodes_file = File.new_for_path (directory.get_path () + "/" + "temp-nodes.data");

		//if (temporary_nodes_file.query_exists ()) {
		//	temporary_nodes_file.delete ();
		//}

		disk_reader_writer = new DiskReaderWriter.from_file (nodes_file);
		//temporary_disk_reader_writer = new DiskReaderWriter.from_file (temporary_nodes_file);
		use_temporary = false;
	}

	public Way create_new_way () {
		return new DiskWay (disk_reader_writer);
	}

	public Node create_new_node () {
		return new DiskNode (disk_reader_writer);
	}

	public void add_node (Node node) throws Error {
		nodes_by_id.set (node.get_id (), node as DiskNode);
	}

	public void add_node_content (NodeContent node_content) throws Error {
		//print (@"$(node_content)\n");
		DiskNode node = new DiskNode (disk_reader_writer);
		node.set_id (node_content.id);
		node.set_location (node_content.get_location ());
		add_node (node);
	}

	public Node get_node_by_id (int64 id) throws Error {
		return nodes_by_id.get (id);
	}

	public void close () throws Error {
		//temporary_disk_reader_writer.close ();
		disk_reader_writer.close ();
		closed = true;
	}

	public void add_way (Way way) throws Error {
		ways.add (way as DiskWay);
	}

	public void finish () throws Error {
		process_ways ();
		write_nodes ();
		print ("\n");
	}

	public int get_number_of_nodes () {
		return nodes_by_id.size;
	}

	public int get_number_of_ways () {
		return ways.size;
	}

	void write_nodes () throws Error {
		int64 current_node_number = 0;
		int64 saved_node_number = 0;
		int node_size = nodes_by_id.values.size;
		foreach (DiskNode node in nodes_by_id.values) {
			current_node_number++;
			if (node.get_connections ().is_empty) {
				continue;
			}
			saved_node_number++;
			if (current_node_number % 1000 == 0) {
				print (@"Writing node: $(current_node_number)/$(node_size) saved: $(saved_node_number)                \r");
			}
			node.write ();
			rtree.insert (BoundingBox.from_point (node.get_location ()), node.get_file_position ());
		}
	}

	void process_ways () throws Error {
		int current_way_number = 0;
		foreach (var way in ways) {
			current_way_number++;
			if (current_way_number % 1000 == 0) {
				print (@"Processing way: $(current_way_number)/$(ways.size)                 \r");
			}
			if (!way.has_tag ("highway")) {
				continue;
			}
			var node_ids = way.get_node_ids ();
			for (int index = 0; index < node_ids.size - 1; index++) {
				var source_node_id = node_ids[index];
				var destination_node_id = node_ids[index+1];

				if (!nodes_by_id.has_key (source_node_id) || !nodes_by_id.has_key (destination_node_id)) {
					continue;
				}

				Node source_node = nodes_by_id.get (source_node_id);
				Node destination_node = nodes_by_id.get (destination_node_id);

				var connection = new DiskConnection (disk_reader_writer);
				connection.set_source (source_node);
				connection.set_destination (destination_node);

				if (way.has_tag ("oneway") &&
					way.get_tag_value ("oneway") == "yes") {
					connection.set_source_not_connectable ();
				}

				if (way.has_tag ("highway")) {
					var grade = evaluate_highway_type (way.get_tag_value ("highway"));
					connection.set_grade (grade);
					assert (connection.get_grade () == grade);
				}

				source_node.add_connection (connection);
				destination_node.add_connection (connection);
			}
		}
	}

	int8 evaluate_highway_type (string highway_type) {
		switch (highway_type) {
			case "motorway":
				return 10;
			case "motorway_link":
				return 10;
			case "trunk":
				return 10;
			case "trunk_link":
				return 10;
			case "primary":
				return 9;
			case "primary_link":
				return 9;
			case "secondary":
				return 9;
			case "secondary_link":
				return 9;
			case "tertiary":
				return 8;
			case "residential":
				return 7;
			case "unclassified":
				return 7;
			case "road":
				return 7;
			case "living_street":
				return 2;
			case "track":
				return 2;
			case "pedestrian":
				return 2;
			case "service":
				return 2;
			case "path":
				return 2;
			case "cycleway":
				return 1;
			case "footway":
				return 1;
			case "bridleway":
				return 1;
			case "steps":
				return 0;
			case "construction":
				return 0;
		}
		return 0;
	}
}

public class DiskNetwork : Object, Network {
	Spatial.RTreeDisk rtree;
	DiskReaderWriter disk_reader_writer;

	bool closed = false;

	public DiskNetwork (File directory) throws Error {
		assert (directory.query_exists ());
		if (directory.query_file_type (FileQueryInfoFlags.NONE) != FileType.DIRECTORY) {
			error ("Not an directory");
		}

		var index_file = File.new_for_path (directory.get_path () + "/" + "index.data");
		if (!index_file.query_exists ()) {
			error ("index.data does not exists\n");
		}
		rtree = new Spatial.RTreeDisk (index_file);

		var nodes_file = File.new_for_path (directory.get_path () + "/" + "nodes.data");

		if (!nodes_file.query_exists ()) {
			error ("Network.data does not exists\n");
		}

		disk_reader_writer = new DiskReaderWriter.from_file (nodes_file);
	}


	public Node? find_nearest_node (Point location) throws Error {
		const double delta = 0.05;

		Spatial.BoundedPointer? nearest = null;
		double nearest_distance = double.MAX;

		BoundingBox search_bounding_box = {
			location.latitude-delta,
			location.longitude-delta,
			location.latitude+delta,
			location.longitude+delta
		};

		foreach (Spatial.BoundedPointer current in rtree.search (search_bounding_box)) {
			double distance = GeoTools.distance_simple (location, current.get_bounding_box ().center ());
			if (distance < nearest_distance) {
				nearest = current;
				nearest_distance = distance;
			}
		}

		if (nearest == null) {
			return null;
		}

		DiskNode node = new DiskNode (disk_reader_writer);
		node.read (nearest.position);
		return node;
	}
}
