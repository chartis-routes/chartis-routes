class NetworkTest : Object {
	public static void test_directory_creation () {
		try {
			var map_directory = File.new_for_path("test_map");
			if (!map_directory.query_exists ()) {
				map_directory.make_directory ();
			}

			ImportNetwork network = new ImportNetwork (map_directory);
			network.close ();

			assert (map_directory.query_exists ());
			assert (File.new_for_path("test_map/index.data").query_exists ());
			assert (File.new_for_path("test_map/nodes.data").query_exists ());
			assert (File.new_for_path("test_map/temp-nodes.data").query_exists ());
		} catch (Error e) {
			error (e.message);
		}
	}

	public static void test_store_node () {
		/*try {

			var map_directory = File.new_for_path("test_map");
			if (!map_directory.query_exists ()) {
				map_directory.make_directory ();
			}

			ImportNetwork network = new ImportNetwork (map_directory);

			Node new_node = network.create_new_node ();
			Point location = {45.0, 12.0};
			new_node.set_id (1);
			new_node.set_location (location);
			network.add_node (new_node);

			Node loaded_node = network.get_node_by_id (1);

			assert (new_node != loaded_node);
			assert (new_node.get_id () == loaded_node.get_id ());
			assert (new_node.get_location () == loaded_node.get_location ());

			network.close ();

		} catch (Error e) {
			error (e.message);
		}*/
	}

	public static void test_network () {
		try {

			var map_directory = File.new_for_path("test_map");
			if (!map_directory.query_exists ()) {
				map_directory.make_directory ();
			}

			ImportNetwork network = new ImportNetwork (map_directory);

			/*Node node1 = network.create_new_node ();
			Point node1_location = {45.0, 12.0};
			node1.set_id (1);
			node1.set_location (node1_location);
			network.add_node (node1);

			Node node2 = network.create_new_node ();
			Point node2_location = {45.1, 12.0};
			node2.set_id (2);
			node2.set_location (node2_location);
			network.add_node (node2);

			Node node3 = network.create_new_node ();
			Point node3_location = {45.0, 12.1};
			node3.set_id (3);
			node3.set_location (node3_location);
			network.add_node (node3);

			Way way_1 = network.create_new_way ();
			way_1.add_node_by_id (1);
			way_1.add_node_by_id (2);
			way_1.add_node_by_id (3);
			network.add_way (way_1);

			network.write ();

			network = new DiskNetwork (file_index, file_network);

			node3 = network.find_nearest_node ({45.0, 12.1});
			assert (node3 != null);

			assert (node3.get_id () == 3);
			assert (node3.get_location () == node3_location);
			assert (node3.get_connections ().size == 1);

			Connection connection;

			connection = node3.get_connections ().first ();
			node2 = connection.get_other_end (node3);
			assert (node2.get_id () == 2);
			assert (node2.get_location () == node2_location);
			assert (node2.get_connections ().size == 2);

			connection = node2.get_connections ().first ();

			node1 = connection.get_other_end (node2);
			assert (node1.get_id () == 1);
			assert (node1.get_location () == node1_location);
			assert (node1.get_connections ().size == 1);

			connection = node2.get_connections ().last ();
			node3 = connection.get_other_end (node2);
			assert (node3.get_id () == 3);*/

			network.close ();

		} catch (Error e) {
			error (e.message);
		}
	}

	public static void add_tests () {
		Test.add_func ("/Network/test_store_node", NetworkTest.test_store_node);
		Test.add_func ("/Network/test_directory_creation", NetworkTest.test_directory_creation);
	}
}
