public class DiskNode : DiskObject, Node {
	public NodeContent content;

	DiskReaderWriter disk_reader_writer;

	Gee.List<DiskConnection> connections = new Gee.ArrayList<DiskConnection>();

	public DiskNode (DiskReaderWriter disk_reader_writer) {
		this.disk_reader_writer = disk_reader_writer;
	}

	public int64 get_id () {
		return content.id;
	}

	public void set_id (int64 id) {
		content.id = id;
	}

	public void set_location (Point location) {
		content.set_location (location);
	}

	public void set_location_latitude (double latitude) {
		content.set_latitude (latitude);
	}

	public void set_location_longitude (double longitude) {
		content.set_longitude (longitude);
	}

	public Point get_location () {
		return content.get_location ();
	}

	public unowned Gee.List<Connection> get_connections () {
		return connections;
	}

	public string to_string () {
		return @"Node($(content.id)) [$(content.get_location ())]";
	}

	public void add_connection (Connection connection) {
		connections.add (connection as DiskConnection);
		content.number_of_connections = (uint16) connections.size;
	}

	public int64 write () throws Error {
		if (get_file_position () < 0) {
			write_new ();
		} else {
			write_existing ();
		}
		return get_file_position ();
	}

	void write_new () throws Error {
		set_file_position (disk_reader_writer.write_new_node (this));
		foreach (var connection in connections) {
			connection.write ();
		}
	}

	void write_existing () throws Error {
		foreach (var connection in connections) {
			if (connection.get_file_position () < 0) {
				return;
			}
		}
		disk_reader_writer.write_node (this, get_file_position ());
	}

	public void read (int64 position) throws Error {
		disk_reader_writer.load_node_at_position (this, position);
		set_file_position (position);
	}
}
