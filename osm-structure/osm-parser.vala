using Xml;

public class OSMFileParser : Object {
	NodeContent current_node;
	Way? current_way;

	string current_key;
	string current_value;

	ImportNetwork network;

	int node_counter = 0;
	int way_counter = 0;

	public OSMFileParser (ImportNetwork network) {
		this.network = network;
	}

	void do_node (string[] attributes) throws GLib.Error {
		node_counter++;
		if (node_counter % 1000 == 0) {
			print (@"Node: $(node_counter)                       \r");
		}
		for (int i=0; i<attributes.length; i+=2) {
			switch (attributes[i]) {
				case "id":
					current_node.id = int64.parse (attributes[i+1]);
					break;
				case "lat":
					double latitude;
					if (double.try_parse (attributes[i+1], out latitude)) {
						current_node.set_latitude (latitude);
					} else {
						error ("Error!");
					}
					break;
				case "lon":
					double longitude;
					if (double.try_parse (attributes[i+1], out longitude)) {
						current_node.set_longitude (longitude);
					} else {
						error ("Error!");
					}
					break;
			}
		}
	}

	void do_tag (string[] attributes) throws GLib.Error {
		for (int i=0; i<attributes.length; i+=2) {
			switch (attributes[i]) {
				case "k":
					current_key = attributes[i+1];
					break;
				case "v":
					current_value = attributes[i+1];
					break;
			}
		}
	}

	void do_way (string[] attributes) throws GLib.Error {
		way_counter++;
		if (way_counter % 1000 == 0) {
			print (@"Ways: $(way_counter)                            \r");
		}
		current_way = network.create_new_way ();
		for (int i=0; i<attributes.length; i+=2) {
			switch (attributes[i]) {
				case "id":
					var id = int64.parse (attributes[i+1]);
					current_way.set_id (id);
					break;
			}
		}
	}

	void do_nd (string[] attributes) throws GLib.Error {
		for (int i=0; i<attributes.length; i+=2) {
			switch (attributes[i]) {
				case "ref":
					int64 node_id = int64.parse (attributes[i+1]);
					current_way.add_node_by_id (node_id);
					break;
			}
		}
	}

	void on_start_element (string name, string[] attributes) {
		try {
			switch (name) {
				case "node":
					do_node (attributes);
					break;
				case "tag":
					do_tag (attributes);
					break;
				case "way":
					do_way (attributes);
					break;
				case "nd":
					do_nd (attributes);
					break;
			}
		} catch (GLib.Error e) {
			error (e.message);
		}
	}

	void on_end_element (string name) {
		try {
			switch (name) {
				case "node":
					network.add_node_content (current_node);
					break;
				case "way":
					if (current_way != null) {
						network.add_way (current_way);
						current_way = null;
					}
					break;
				case "tag":
					if (current_way != null) {
						current_way.add_tag (current_key, current_value);
					}
					break;
			}
		} catch (GLib.Error e) {
			error (e.message);
		}
	}

	public void parse (string filename) {
		var handler = SAXHandler ();

		handler.startElement = on_start_element;
		handler.endElement = on_end_element;

		var result = handler.user_parse_file (this, filename);

		if (result < 0) {
			print ("Error: %d\n", result);
		}
	}
}
