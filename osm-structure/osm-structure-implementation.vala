public abstract class DiskObject : Object {
	int64 file_position = -1;

	public void set_file_position (int64 position) {
		file_position = position;
	}

	public int64 get_file_position () {
		return file_position;
	}
}
