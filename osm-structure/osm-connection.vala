public class DiskConnection : DiskObject, Connection {
	public ConnectionContent content;
	double distance = double.MIN;
	bool needs_distance_calculation = true;

	DiskNode node1;
	DiskNode node2;

	DiskReaderWriter disk_reader_writer;

	public DiskConnection (DiskReaderWriter disk_reader_writer) {
		this.disk_reader_writer = disk_reader_writer;
	}

	public void set_source (Node node) throws Error {
		node1 = node as DiskNode;
	}

	public void set_destination (Node node) throws Error {
		node2 = node as DiskNode;
	}

	public Node get_source () throws Error {
		if (node1 == null) {
			node1 = new DiskNode (disk_reader_writer);
			node1.read (content.node1);
		}
		return node1;
	}

	public Node get_destination () throws Error {
		if (node2 == null) {
			node2 = new DiskNode (disk_reader_writer);
			node2.read (content.node2);
		}
		return node2;
	}

	public bool connects_with (Node node) throws Error {
		return (get_source ().get_id () == node.get_id () || get_destination ().get_id () == node.get_id ());
	}

	public Node? get_other_end (Node node) throws Error {
		int64 node_id = node.get_id ();
		if (get_source ().get_id () == node_id) {
			return get_destination ();
		} else if (get_destination ().get_id () == node_id) {
			return get_source ();
		} else {
			return null;
		}
	}

	public bool is_connectable (Node node) throws Error {
		int64 node_id = node.get_id ();
		if (get_destination ().get_id () == node_id) {
			return (content.flags & 0x02) == 0;
		} else if (get_source ().get_id () == node_id) {
			return (content.flags & 0x01) == 0;
		}
		return false;
	}

	public void write () throws Error {
		if (node1.get_file_position () < 0 || node2.get_file_position () < 0) {
			return;
		}

		content.node1 = node1.get_file_position ();
		content.node2 = node2.get_file_position ();

		if (get_file_position () < 0) {
			var file_position = disk_reader_writer.write_new_connection (this);
			set_file_position (file_position);
			node1.write ();
			node2.write ();
		} else {
			disk_reader_writer.write_connection (this, get_file_position ());
		}
	}

	public void set_grade (int8 grade) {
		content.flags = content.flags | (grade << 2);
	}

	public int8 get_grade () {
		var grade = ((content.flags >> 2) & 0xFF);
		return (int8) grade;
	}

	public void set_source_not_connectable () {
		content.flags = content.flags | 0x01;
	}

	public void set_destination_not_connectable () {
		content.flags = content.flags | 0x02;
	}

	public void read (int64 position) throws Error {
		disk_reader_writer.load_connection_at_position (this, position);
		set_file_position (position);
	}

	public double get_distance () throws Error {
		if (needs_distance_calculation) {
			distance = GeoTools.distance_simple (node1.get_location (), node2.get_location ());
			needs_distance_calculation = false;
		}
		return distance;
	}
}
