public class DiskWay : Object, Way {
	public struct WayContent {
		public int64 id;
	}

	WayContent content;
	Gee.List<int64?> nodes = new Gee.ArrayList<int64?> ();
	Gee.Map<string, string> tags = new Gee.HashMap<string, string> (str_hash, str_equal, str_equal);
	DiskReaderWriter disk_reader_writer;

	public DiskWay (DiskReaderWriter disk_reader_writer) {
		this.disk_reader_writer = disk_reader_writer;
	}

	public void set_id (int64 id) {
		content.id = id;
	}

	public int64 get_id () {
		return content.id;
	}

	public void add_node_by_id (int64 id) {
		nodes.add (id);
	}

	public unowned Gee.List<int64?> get_node_ids () {
		return nodes;
	}

	public void add_tag (string key, string value) {
		tags[key] = value;
	}

	public bool has_tag (string key) {
		return tags.has_key (key);
	}

	public string get_tag_value (string key) {
		return tags[key];
	}
}
