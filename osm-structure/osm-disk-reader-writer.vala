public class DiskReaderWriter : Object {
	InputStream input_stream;
	OutputStream output_stream;

	DataOutputStream data_output_stream;
	Seekable output_seekable;

	public DiskReaderWriter.from_file  (File network_file) throws Error {
		FileIOStream io_stream;
		if (network_file.query_exists ()) {
			io_stream = network_file.open_readwrite ();
		} else {
			io_stream = network_file.create_readwrite (FileCreateFlags.REPLACE_DESTINATION);
		}
		input_stream = io_stream.get_input_stream ();
		output_stream = io_stream.get_output_stream ();

		if (input_stream is Seekable == false) {
			throw new IOError.FAILED ("Seekable not supported!");
		}
		if (output_stream is Seekable == false) {
			throw new IOError.FAILED ("Seekable not supported!");
		}

		data_output_stream = new DataOutputStream (output_stream);
		data_output_stream.set_close_base_stream (false);
		output_seekable = output_stream as Seekable;
	}

	Seekable get_input_seekable () {
		return input_stream as Seekable;
	}

	public int64 write_new_node (DiskNode node) throws Error {
		output_seekable.seek (0, SeekType.END);
		int64 position = output_seekable.tell ();
		write_node_only (node, position);
		return position;
	}

	public void write_node (DiskNode node, int64 position) throws Error {
		output_seekable.seek (position, SeekType.SET);
		write_node_only (node, position);
	}

	public void write_node_only (DiskNode node, int64 position) throws Error {
		NodeContent* node_content_pointer = &node.content;
		unowned uint8[] byte_array = (uint8[]) node_content_pointer;
		byte_array.length = (int) sizeof (NodeContent);
		size_t written;
		output_stream.write_all (byte_array, out written);

		foreach (var connection in node.get_connections ()) {
			var disk_connection = connection as DiskConnection;
			var file_position = disk_connection.get_file_position ();
			data_output_stream.put_int64 (file_position);
		}
	}

	public int64 write_new_node_content (NodeContent* node_content_pointer) throws Error {
		output_seekable.seek (0, SeekType.END);
		int64 file_location = output_seekable.tell ();

		unowned uint8[] byte_array = (uint8[]) node_content_pointer;
		byte_array.length = (int) sizeof (NodeContent);
		size_t written;
		output_stream.write_all (byte_array, out written);
		return file_location;
	}

	public int64 write_new_connection (DiskConnection connection) throws Error {
		output_seekable.seek (0, SeekType.END);
		int64 position = output_seekable.tell ();

		write_connection (connection, position);
		return position;
	}

	public void write_connection (DiskConnection connection, int64 position) throws Error {
		output_seekable.seek (position, SeekType.SET);

		ConnectionContent* connection_content_pointer = &connection.content;
		unowned uint8[] byte_array = (uint8[]) connection_content_pointer;
		byte_array.length = (int) sizeof (ConnectionContent);
		size_t written;
		output_stream.write_all (byte_array, out written);
	}

	public void load_connection_at_position (DiskConnection connection, int64 position) throws Error {
		get_input_seekable ().seek (position, SeekType.SET);

		uint8[] connection_content_byte_array = new uint8[sizeof (ConnectionContent)];
		size_t read;
		bool result = input_stream.read_all (connection_content_byte_array, out read);
		if (!result || read == 0) {
			error ("Could not read!");
		}

		ConnectionContent* connection_content = (ConnectionContent*) connection_content_byte_array;
		connection.content = *connection_content;
	}

	public void load_node_at_position (DiskNode node, int64 position) throws Error {
		get_input_seekable ().seek (position, SeekType.SET);

		uint8[] node_content_byte_array = new uint8[sizeof (NodeContent)];
		size_t read;
		bool result = input_stream.read_all (node_content_byte_array, out read);
		if (!result || read == 0) {
			error ("Could not read!");
		}

		NodeContent* node_content = (NodeContent*) node_content_byte_array;
		node.content = *node_content;
		//print ("Loaded node content: %s %s %d \n",node.content.id.to_string (), node.content.location.to_string (), node.content.number_of_connections);

		var data_input_stream = new DataInputStream (input_stream);
		data_input_stream.set_close_base_stream (false);

		int64 connection_file_position;
		DiskConnection connection;

		uint16 number_of_connections = node.content.number_of_connections;

		for (int i=0; i<number_of_connections; i++) {
			connection = new DiskConnection (this);
			connection_file_position = data_input_stream.read_int64 ();
			if (connection_file_position < 0) {
				error ("Position is not valid!");
			}
			connection.set_file_position (connection_file_position);
			//print ("P: %s\n", connection_file_position.to_string ());
			node.add_connection (connection);
		}

		foreach (var each in node.get_connections ()) {
			var disk_connection = each as DiskConnection;
			disk_connection.read (disk_connection.get_file_position ());
		}

		data_input_stream.close ();
	}

	public void close () throws Error {
		data_output_stream.close ();
		input_stream.close ();
		output_stream.close ();
	}
}
