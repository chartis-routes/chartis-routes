valac-0.14 \
	--pkg libxml-2.0 \
	--pkg cairo \
	--pkg gee-1.0 \
	--pkg gio-2.0 \
	--pkg gtk+-2.0 \
	base/base-structures.vala \
	base/geo-tools.vala \
	osm-structure/osm-structure.vala \
	osm-structure/osm-parser2.vala \
	osm-structure/osm-node.vala \
	osm-structure/osm-way.vala \
	osm-structure/osm-connection.vala \
	osm-structure/osm-network.vala \
	osm-structure/osm-structure-implementation.vala \
	osm-structure/osm-disk-reader-writer.vala \
	disk-r-tree/disk-r-tree.vala \
	disk-r-tree/disk-r-tree-node.vala \
	disk-r-tree/disk-r-tree-disk-handler.vala \
	osm-to-network-converter2.vala \
	-o osm-to-network-converter2
