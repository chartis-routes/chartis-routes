valac-0.14 \
	--pkg libxml-2.0 \
	--pkg gee-1.0 \
	--pkg gtk+-3.0 \
	-o chartis-find \
	chartis-find.vala \
	route-finding/route-finder-interface.vala \
	route-finding/base-route-finder-short.vala \
	route-finding/base-route-finder-fast.vala \
	route-finding/distance-based-path-skipping-route-finder.vala \
	route-finding/path-skipping-route-finder.vala \
	route-finding/distance-based-route-finder.vala \
	route-finding/two-point-route-finder-short.vala \
	route-finding/two-point-route-finder-fast.vala \
	base/base-structures.vala \
	base/geo-tools.vala \
	osm-structure/osm-structure.vala \
	osm-structure/osm-parser.vala \
	osm-structure/osm-node.vala \
	osm-structure/osm-way.vala \
	osm-structure/osm-connection.vala \
	osm-structure/osm-network.vala \
	osm-structure/osm-structure-implementation.vala \
	osm-structure/osm-disk-reader-writer.vala \
	disk-r-tree/disk-r-tree.vala \
	disk-r-tree/disk-r-tree-node.vala \
	disk-r-tree/disk-r-tree-disk-handler.vala
