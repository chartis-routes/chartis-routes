valac-0.12 \
	--pkg libxml-2.0 \
	--pkg gee-1.0 \
	--pkg gtk+-3.0 \
	base/base-structures.vala \
	base/geo-tools.vala \
	base/disk-tools.vala \
	base/disk-tools-test.vala \
	base/base-structures-test.vala \
	base/geo-tools-test.vala \
	disk-r-tree/disk-r-tree.vala \
	disk-r-tree/disk-r-tree-node.vala \
	disk-r-tree/disk-r-tree-disk-handler.vala \
	disk-r-tree/disk-r-tree-test.vala \
	route-finding/route-finder-interface.vala \
	route-finding/base-route-finder-short.vala \
	route-finding/two-point-route-finder-short.vala \
	osm-structure/osm-structure.vala \
	osm-structure/osm-parser.vala \
	osm-structure/osm-node.vala \
	osm-structure/osm-way.vala \
	osm-structure/osm-connection.vala \
	osm-structure/osm-network.vala \
	osm-structure/osm-structure-implementation.vala \
	osm-structure/osm-disk-reader-writer.vala \
	osm-structure/disk-writer-test.vala \
	chartis-routes-tests.vala \
	-o chartis-routes-tests
