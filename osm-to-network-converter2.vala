
public class OsmToNetworkConverter : Object {
	File file_osm;

	public OsmToNetworkConverter (File file_osm) throws Error {
		assert (file_osm.query_exists ());
		this.file_osm = file_osm;
	}

	public void convert (File map_directory) throws Error {
		var timer = new Timer ();
		var parser = new OSMFileParserFirstPass ();
		print ("Parsing %s...\n", file_osm.get_path ());
		parser.parse (file_osm.get_path ());
		print ("Completed in %.4f sec\n", timer.elapsed ());
	}
}

int main (string[] args) {
	try {
		if (args.length != 2) {
			return 1;
		}

		var file_osm = File.new_for_path(args[1]);
		if (!file_osm.query_exists ()) {
			print ("File does not exists!\n");
			return 1;
		}

		var map_directory = File.new_for_path("routing-data");
		if (!map_directory.query_exists ()) {
			map_directory.make_directory ();
		}

		var converter = new OsmToNetworkConverter (file_osm);
		converter.convert (map_directory);

	} catch (Error e) {
		error (e.message);
	}
	return 0;
}
