
public class TestBoundingBox : Object {

	public static void assert_bounding_box (BoundingBox result, BoundingBox expected) {
		if (result != expected) {
			error ("Result: %s Expected: %s", result.to_string (), expected.to_string ());
		}
	}

	public static void test_create_instance () {
		BoundingBox bbox;
		BoundingBox bbox1234 = BoundingBox.full (1.0, 2.0, 3.0, 4.0);
		BoundingBox bbox1212 = BoundingBox.full (1.0, 2.0, 1.0, 2.0);

		bbox = BoundingBox.full (1.0, 2.0, 3.0, 4.0);
		assert (bbox == bbox1234);
		assert (bbox.south == 1.0);
		assert (bbox.west == 2.0);
		assert (bbox.north == 3.0);
		assert (bbox.east == 4.0);

		bbox = {1.0, 2.0, 3.0, 4.0};
		assert (bbox == bbox1234);
		assert (bbox.south == 1.0);
		assert (bbox.west == 2.0);
		assert (bbox.north == 3.0);
		assert (bbox.east == 4.0);

		bbox = BoundingBox () { south = 1.0, west = 2.0, north = 3.0, east = 4.0 };
		assert (bbox == bbox1234);
		assert (bbox.south == 1.0);
		assert (bbox.west == 2.0);
		assert (bbox.north == 3.0);
		assert (bbox.east == 4.0);

		bbox = BoundingBox.from_point ({1.0, 2.0});
		assert (bbox == bbox1212);
		assert (bbox.south == 1.0);
		assert (bbox.west == 2.0);
		assert (bbox.north == 1.0);
		assert (bbox.east == 2.0);
	}

	public static void test_includes () {
		BoundingBox bbox;
		bbox = {0.0, 0.0, 2.0, 2.0};

		assert (bbox.includes ({0.0, 0.0, 2.0, 2.0}));
		assert (bbox.includes ({1.0, 1.0, 1.0, 1.0}));
		assert (bbox.includes ({0.0, 0.0, 0.0, 0.0}));
		assert (bbox.includes ({0.0, 0.0, 1.0, 1.0}));
		assert (bbox.includes ({2.0, 2.0, 2.0, 2.0}));
		assert (bbox.includes ({2.0, 2.0, 1.0, 1.0}));
		assert (!bbox.includes ({3.0, 3.0, 3.0, 3.0}));
		assert (!bbox.includes ({0.0, 0.0, 3.0, 3.0}));
		assert (!bbox.includes ({2.0, 2.0, 2.0, 3.0}));
	}

	public static void test_intersection () {
		BoundingBox bbox;
		bbox = {1.0, 1.0, 3.0, 3.0};

		assert_bounding_box (bbox.intersect ({1.0, 1.0, 3.0, 3.0}), BoundingBox.full (1.0, 1.0, 3.0, 3.0));
		assert_bounding_box (bbox.intersect ({1.0, 1.0, 1.0, 1.0}), BoundingBox.full (1.0, 1.0, 1.0, 1.0));
		assert_bounding_box (bbox.intersect ({2.0, 2.0, 2.0, 2.0}), BoundingBox.full (2.0, 2.0, 2.0, 2.0));
		assert_bounding_box (bbox.intersect ({3.0, 3.0, 3.0, 3.0}), BoundingBox.full (3.0, 3.0, 3.0, 3.0));
		assert_bounding_box (bbox.intersect ({1.0, 1.0, 2.0, 2.0}), BoundingBox.full (1.0, 1.0, 2.0, 2.0));
		assert_bounding_box (bbox.intersect ({3.0, 3.0, 2.0, 2.0}), BoundingBox.invalid ());
		assert_bounding_box (bbox.intersect ({4.0, 4.0, 4.0, 4.0}), BoundingBox.invalid ());
		assert_bounding_box (bbox.intersect ({4.0, 4.0, 6.0, 6.0}), BoundingBox.invalid ());

		assert_bounding_box (bbox.intersect ({0.0, 0.0, 2.0, 2.0}), BoundingBox.full (1.0, 1.0, 2.0, 2.0));
		assert_bounding_box (bbox.intersect ({2.0, 2.0, 4.0, 4.0}), BoundingBox.full (2.0, 2.0, 3.0, 3.0));

		bbox = {1.0, 0.0, 3.0, 5.0};

		assert_bounding_box (bbox.intersect ({0.0, 1.0, 5.0, 3.0}), BoundingBox.full (1.0, 1.0, 3.0, 3.0));
	}

	public static void test_merge () {
		BoundingBox bbox;
		BoundingBox result;
		bbox = {0.0, 0.0, 2.0, 2.0};

		result = bbox.merge ({1.0, 1.0, 1.0, 1.0});
		assert (result == BoundingBox.full (0.0, 0.0, 2.0, 2.0));

		result = bbox.merge ({0.0, 0.0, 1.0, 1.0});
		assert (result == BoundingBox.full (0.0, 0.0, 2.0, 2.0));

		result = bbox.merge ({-1.0, -1.0, 1.0, 1.0});
		assert (result == BoundingBox.full (-1.0, -1.0, 2.0, 2.0));

		result = bbox.merge ({-1.0, -1.0, 3.0, 3.0});
		assert (result == BoundingBox.full (-1.0, -1.0, 3.0, 3.0));

		result = bbox.merge ({2.0, 2.0, 3.0, 3.0});
		assert (result == BoundingBox.full (0.0, 0.0, 3.0, 3.0));

		result = bbox.merge ({3.0, 3.0, 4.0, 4.0});
		assert (result == BoundingBox.full (0.0, 0.0, 4.0, 4.0));
	}

	public static void test_merge_check_copy () {
		BoundingBox bbox1;
		BoundingBox bbox2;
		BoundingBox result;
		bbox1 = {0.0, 0.0, 0.0, 0.0};
		bbox2 = {1.0, 1.0, 1.0, 1.0};

		result = bbox1.merge (bbox2);
		assert (result == BoundingBox.full (0.0, 0.0, 1.0, 1.0));
		assert (bbox1 == BoundingBox.full (0.0, 0.0, 0.0, 0.0));
		assert (bbox2 == BoundingBox.full (1.0, 1.0, 1.0, 1.0));
	}

	public static void test_area () {

	}
}
