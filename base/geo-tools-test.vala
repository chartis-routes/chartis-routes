
public class GeoToolsTest : Object {

	public static void assert_distance (double expected_distance, double distance) {
		double delta = Math.fabs (expected_distance - distance);
		//print ("Distance is: %f expected %f\n", distance, expected_distance);
		assert (delta < 0.01);
	}

	public static void test_distance_simple () {
		assert_distance (417.83, GeoTools.distance_simple_lat_lon (50.11222, 8.68194, 52.52222, 13.29750));
		assert_distance (7166.35, GeoTools.distance_simple_lat_lon (33.431441, -5.976563, 55.578345, 86.835938));
		assert_distance (5649.28, GeoTools.distance_simple_lat_lon (-26.431228, 32.343750, -53.330873, -26.718750));
	}

	public static void test_distance_exact () {
		assert_distance (417.83, GeoTools.distance_exact_lat_lon (50.11222, 8.68194, 52.52222, 13.29750));
		assert_distance (7166.35, GeoTools.distance_exact_lat_lon (33.431441, -5.976563, 55.578345, 86.835938));
		assert_distance (5649.28, GeoTools.distance_exact_lat_lon (-26.431228, 32.343750, -53.330873, -26.718750));
	}
}
