public class BetterDataOutputStream : DataOutputStream {

	public BetterDataOutputStream (OutputStream base_stream) {
		Object (base_stream : base_stream);
	}

	public bool put_double (double data, GLib.Cancellable? cancellable = null) throws GLib.IOError {
		double* data_pointer = &data;
		unowned uint8[] byte_array = (uint8[]) data_pointer;
		byte_array.length = (int) sizeof(double);
		size_t written;
		return write_all (byte_array, out written);
	}

	/*void write_variant (OutputStream stream, Variant variant) throws IOError {
		unowned uint8[] buffer = (uint8[]) variant.get_data();
		buffer.length = (int) variant.get_size();
		stream.write_all (buffer, null);
	}

	void write_node (OutputStream stream, Node node) throws IOError {
		var id = new Variant.uint64 (node.id);
		var longitude = new Variant.double (node.location.longitude);
		var latitude = new Variant.double (node.location.latitude);
		var number_of_connections = (uint16) node.connections.size;
		var tuple = new Variant.tuple (new Variant[] {id, longitude, latitude, number_of_connections});
		write_variant (stream, tuple);
	}*/
}

public class BetterDataInputStream : DataInputStream {

	public BetterDataInputStream (InputStream base_stream) {
		Object (base_stream : base_stream);
	}

	public double read_double (GLib.Cancellable? cancellable = null) throws GLib.IOError {
		size_t read;
		uint8[] data = new uint8[sizeof(double)];
		bool result = read_all (data, out read);
		if (result == false) {
			throw new IOError.FAILED ("Could not read from stream.");
		}
		double* data_pointer = (double*) data;
		return *data_pointer;
	}
}

public class Cache<K,V> : Object {
	Gee.Map<K,Container<V>?> cache_map = new Gee.HashMap<K,unowned Container<V>?> ();

	int soft_limit;
	int hard_limit;

	uint64 counter = 0;

	[Compact]
	class Container<V> {
		public V value;
		public uint64 count;
		public Container (uint64 count, owned V value) {
			this.value = (owned) value;
			this.count = count;
		}
	}

	public Cache (int soft_limit, int hard_limit) {
		this.soft_limit = soft_limit;
		this.hard_limit = hard_limit;
	}

	public bool has_key (K key) {
		return cache_map.has_key (key);
	}

	public new V @get (K key) {
		var container = cache_map.get (key);
		container.count++;
		cache_map.set (key, container);
		return container.value;
	}

	public new void @set (K key, V value) {
		var container = new Container<V> (counter++, value);
		cache_map.set (key, (owned) container);

		if (cache_map.size >= hard_limit) {
			collect_garbage ();
		}
	}

	void collect_garbage () {
	}

}
