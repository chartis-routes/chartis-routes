
public struct Point {
	public double latitude;
	public double longitude;

	public Point (double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public string to_string () {
		return "Point [%.5f %.5f]".printf (latitude, longitude);
	}

	public double distance (Point point) {
		return GeoTools.distance_simple (this, point);
	}

	public int compare_to (Point point) {
		if (this == point) {
			return 0;
		} else if (this.distance ({0.0, 0.0}) < point.distance ({0.0, 0.0})) {
			return -1;
		} else {
			return 1;
		}
	}
}

public struct BoundingBox {
	public double south;
	public double west;
	public double north;
	public double east;

	public BoundingBox.full (double south, double west, double north, double east) {
		this.south = south;
		this.west = west;
		this.north = north;
		this.east = east;
	}

	public BoundingBox.from_point (Point point) {
		this.south = point.latitude;
		this.west = point.longitude;
		this.north = point.latitude;
		this.east = point.longitude;
	}

	public BoundingBox.invalid () {
		this.south = double.INFINITY;
		this.west = double.INFINITY;
		this.north = -double.INFINITY;
		this.east = -double.INFINITY;
	}

	public BoundingBox intersect (BoundingBox bbox) {
		BoundingBox new_box = {
			double.max (this.south, bbox.south),
			double.max (this.west, bbox.west),
			double.min (this.north, bbox.north),
			double.min (this.east, bbox.east)
		};

		if (new_box.south > new_box.north || new_box.west > new_box.east) {
			return BoundingBox.full (double.INFINITY, double.INFINITY, -double.INFINITY, -double.INFINITY);
		}
		return new_box;
	}

	public bool includes (BoundingBox bbox) {
		return
			(bbox.north <= north && bbox.south >= south &&
			bbox.east <= east && bbox.west >= west);
	}

	public bool includes_point (Point point) {
		return
			(point.latitude <= north && point.latitude >= south &&
			point.longitude <= east && point.longitude >= west);
	}

	public bool overlaps (BoundingBox bbox) {
		return
			includes_point (bbox.south_west_point ()) ||
			includes_point (bbox.south_east_point ()) ||
			includes_point (bbox.north_west_point ()) ||
			includes_point (bbox.north_east_point ());
	}

	public void extend (BoundingBox bbox) {
		if (bbox.south < this.south) {
			this.south = bbox.south;
		}
		if (bbox.west < this.west) {
			this.west = bbox.west;
		}
		if (bbox.north > this.north) {
			this.north = bbox.north;
		}
		if (bbox.east > this.east) {
			this.east = bbox.east;
		}
	}

	public BoundingBox merge (BoundingBox bbox) {
		BoundingBox merged_bbox = this;
		merged_bbox.extend (bbox);
		return merged_bbox;
	}

	public double area () {
		return width () * height ();
	}

	public double area_size_with (BoundingBox bbox) {
		return merge (bbox).area ();
	}

	public Point center () {
		return {(north+south)/2.0, (west+east)/2.0};
	}

	public Point low () {
		return {south, west};
	}

	public Point high () {
		return {north, east};
	}

	public Point south_west_point () {
		return {south, west};
	}

	public Point south_east_point () {
		return {south, east};
	}

	public Point north_west_point () {
		return {north, west};
	}

	public Point north_east_point () {
		return {north, east};
	}

	public double height () {
		return north - south;
	}

	public double width () {
		return east - west;
	}

	public string to_string () {
		return "BBox [S:%.5f W:%.5f N:%.5f E:%.5f]".printf (south, west, north, east);
	}
}
