using Math;

public class GeoTools {
	public static double to_rad (double value) {
		return (value * PI) / 180.0;
	}

	public static double distance_exact_lat_lon (double lat1, double lon1, double lat2, double lon2) {
		double a1 = sin (to_rad((lat2-lat1)/2));
		double a2 = sin (to_rad((lon2-lon1)/2));
		double a = (a1*a1) + cos (to_rad(lat1)) * cos (to_rad(lat2)) * (a2*a2);
		double c = 2 * atan2 (sqrt (a), sqrt (1.0-a));
		return 6371.0 * c;
	}

	public static double distance_simple_lat_lon (double lat1, double lon1, double lat2, double lon2) {
		return 6371.0 * acos (sin (to_rad(lat1)) * sin (to_rad(lat2)) + cos (to_rad(lat1)) * cos (to_rad(lat2)) * cos (to_rad(lon2-lon1)));
	}

	public static double distance_simple (Point location1, Point location2) {
		return distance_simple_lat_lon (location1.latitude, location1.longitude, location2.latitude, location2.longitude);
	}

	public static double distance2 (double lat1, double lon1, double lat2, double lon2) {
		var one = sin(to_rad(lat1)) * sin(to_rad(lat2));
		var two = cos(to_rad(lat1)) * cos(to_rad(lat2)) * cos(to_rad(lon2-lon1));
		var result = acos (one + two);
		return result * 6371.0;
	}

}
